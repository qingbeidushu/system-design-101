<div class="Box-sc-g0xbh4-0 bJMeLZ js-snippet-clipboard-copy-unpositioned" data-hpc="true"><article class="markdown-body entry-content container-lg" itemprop="text"><p dir="auto">
  <a href="https://blog.bytebytego.com/?utm_source=site" rel="nofollow"><img src="/ByteByteGoHq/system-design-101/raw/main/images/banner.jpg" style="max-width: 100%;"> </a>
</p>
<p align="center" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
  【
   </font></font><a href="https://www.youtube.com/channel/UCZgt6AzoyjslHTC9dz0UoTw" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
    👨🏻&zwj;💻 YouTube
  </font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> | 
   </font></font><a href="https://blog.bytebytego.com/?utm_source=site" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
    📮 新闻通讯
  </font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">】
</font></font></p>
<p dir="auto"><a href="https://trendshift.io/repositories/3709" rel="nofollow"><img src="https://camo.githubusercontent.com/4c030ca645442d7f8593a8b87a248a54ad9a1f4190b3122f46506c71e00ab296/68747470733a2f2f7472656e6473686966742e696f2f6170692f62616467652f7265706f7369746f726965732f33373039" alt="ByteByteGoHq%2F系统设计-101 | Trendshift" style="width: 250px; height: 55px; max-width: 100%;" width="250" height="55" data-canonical-src="https://trendshift.io/api/badge/repositories/3709"></a></p>
<div class="markdown-heading" dir="auto"><h1 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">系统设计101</font></font></h1><a id="user-content-system-design-101" class="anchor" aria-label="永久链接：系统设计101" href="#system-design-101"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用视觉和简单的术语解释复杂的系统。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">无论您是在准备系统设计面试，还是只是想了解系统底层的工作原理，我们都希望这个存储库能够帮助您实现这一目标。</font></font></p>
<div class="markdown-heading" dir="auto"><h1 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">目录</font></font></h1><a id="user-content-table-of-contents" class="anchor" aria-label="固定链接：目录" href="#table-of-contents"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>

<ul dir="auto">
<li><a href="#communication-protocols"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">通信协议</font></font></a>
<ul dir="auto">
<li><a href="#rest-api-vs-graphql"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">REST API 与 GraphQL</font></font></a></li>
<li><a href="#how-does-grpc-work"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">gRPC 如何工作？</font></font></a></li>
<li><a href="#what-is-a-webhook"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">什么是 webhook？</font></font></a></li>
<li><a href="#how-to-improve-api-performance"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如何提高API性能？</font></font></a></li>
<li><a href="#http-10---http-11---http-20---http-30-quic"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">HTTP 1.0 -&gt; HTTP 1.1 -&gt; HTTP 2.0 -&gt; HTTP 3.0（QUIC）</font></font></a></li>
<li><a href="#soap-vs-rest-vs-graphql-vs-rpc"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">SOAP、REST、GraphQL、RPC 的比较</font></font></a></li>
<li><a href="#code-first-vs-api-first"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码优先与 API 优先</font></font></a></li>
<li><a href="#http-status-codes"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">HTTP 状态代码</font></font></a></li>
<li><a href="#what-does-api-gateway-do"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">API 网关起什么作用？</font></font></a></li>
<li><a href="#how-do-we-design-effective-and-safe-apis"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">我们如何设计有效且安全的 API？</font></font></a></li>
<li><a href="#tcpip-encapsulation"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">TCP/IP 封装</font></font></a></li>
<li><a href="#why-is-nginx-called-a-reverse-proxy"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">为什么 Nginx 被称为“反向”代理？</font></font></a></li>
<li><a href="#what-are-the-common-load-balancing-algorithms"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">常见的负载均衡算法有哪些？</font></font></a></li>
<li><a href="#url-uri-urn---do-you-know-the-differences"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">URL、URI、URN——您知道它们之间的区别吗？</font></font></a></li>
</ul>
</li>
<li><a href="#cicd"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">持续集成/持续交付</font></font></a>
<ul dir="auto">
<li><a href="#cicd-pipeline-explained-in-simple-terms"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">简单解释 CI/CD 管道</font></font></a></li>
<li><a href="#netflix-tech-stack-cicd-pipeline"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Netflix 技术栈（CI/CD 管道）</font></font></a></li>
</ul>
</li>
<li><a href="#architecture-patterns"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">架构模式</font></font></a>
<ul dir="auto">
<li><a href="#mvc-mvp-mvvm-mvvm-c-and-viper"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MVC、MVP、MVVM、MVVM-C 和 VIPER</font></font></a></li>
<li><a href="#18-key-design-patterns-every-developer-should-know"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">每个开发人员都应该知道的 18 个关键设计模式</font></font></a></li>
</ul>
</li>
<li><a href="#database"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">数据库</font></font></a>
<ul dir="auto">
<li><a href="#a-nice-cheat-sheet-of-different-databases-in-cloud-services"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">一份关于云服务中不同数据库的详细清单</font></font></a></li>
<li><a href="#8-data-structures-that-power-your-databases"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">为数据库提供动力的 8 种数据结构</font></font></a></li>
<li><a href="#how-is-an-sql-statement-executed-in-the-database"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">SQL语句在数据库中是如何执行的？</font></font></a></li>
<li><a href="#cap-theorem"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">CAP 定理</font></font></a></li>
<li><a href="#types-of-memory-and-storage"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">内存和存储的类型</font></font></a></li>
<li><a href="#visualizing-a-sql-query"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">可视化 SQL 查询</font></font></a></li>
<li><a href="#sql-language"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">SQL 语言</font></font></a></li>
</ul>
</li>
<li><a href="#cache"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">缓存</font></font></a>
<ul dir="auto">
<li><a href="#data-is-cached-everywhere"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">数据缓存在各处</font></font></a></li>
<li><a href="#why-is-redis-so-fast"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Redis 为何这么快？</font></font></a></li>
<li><a href="#how-can-redis-be-used"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Redis 如何使用？</font></font></a></li>
<li><a href="#top-caching-strategies"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">顶级缓存策略</font></font></a></li>
</ul>
</li>
<li><a href="#microservice-architecture"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">微服务架构</font></font></a>
<ul dir="auto">
<li><a href="#what-does-a-typical-microservice-architecture-look-like"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">典型的微服务架构是什么样的？</font></font></a></li>
<li><a href="#microservice-best-practices"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">微服务最佳实践</font></font></a></li>
<li><a href="#what-tech-stack-is-commonly-used-for-microservices"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">微服务通常使用什么技术栈？</font></font></a></li>
<li><a href="#why-is-kafka-fast"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">为什么Kafka速度很快</font></font></a></li>
</ul>
</li>
<li><a href="#payment-systems"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">支付系统</font></font></a>
<ul dir="auto">
<li><a href="#how-to-learn-payment-systems"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如何学习支付系统？</font></font></a></li>
<li><a href="#why-is-the-credit-card-called-the-most-profitable-product-in-banks-how-does-visamastercard-make-money"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">信用卡为何被称为“银行最赚钱的产品”？VISA/Mastercard如何赚钱？</font></font></a></li>
<li><a href="#how-does-visa-work-when-we-swipe-a-credit-card-at-a-merchants-shop"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">当我们在商家店铺刷信用卡时 VISA 如何运作？</font></font></a></li>
<li><a href="#payment-systems-around-the-world-series-part-1-unified-payments-interface-upi-in-india"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">全球支付系统系列（第 1 部分）：印度的统一支付接口 (UPI)</font></font></a></li>
</ul>
</li>
<li><a href="#devops"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">DevOps</font></font></a>
<ul dir="auto">
<li><a href="#devops-vs-sre-vs-platform-engineering-what-is-the-difference"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">DevOps 与 SRE 与平台工程。有什么区别？</font></font></a></li>
<li><a href="#what-is-k8s-kubernetes"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">什么是 k8s (Kubernetes)？</font></font></a></li>
<li><a href="#docker-vs-kubernetes-which-one-should-we-use"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Docker 与 Kubernetes。我们应该使用哪一个？</font></font></a></li>
<li><a href="#how-does-docker-work"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Docker 如何工作？</font></font></a></li>
</ul>
</li>
<li><a href="#git"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">胃肠道疾病</font></font></a>
<ul dir="auto">
<li><a href="#how-git-commands-work"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Git 命令如何工作</font></font></a></li>
<li><a href="#how-does-git-work"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Git 如何工作？</font></font></a></li>
<li><a href="#git-merge-vs-git-rebase"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Git merge 与 Git rebase</font></font></a></li>
</ul>
</li>
<li><a href="#cloud-services"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">云服务</font></font></a>
<ul dir="auto">
<li><a href="#a-nice-cheat-sheet-of-different-cloud-services-2023-edition"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">不同云服务的实用小抄（2023 年版）</font></font></a></li>
<li><a href="#what-is-cloud-native"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">什么是云原生？</font></font></a></li>
</ul>
</li>
<li><a href="#developer-productivity-tools"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">开发人员生产力工具</font></font></a>
<ul dir="auto">
<li><a href="#visualize-json-files"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">可视化 JSON 文件</font></font></a></li>
<li><a href="#automatically-turn-code-into-architecture-diagrams"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">自动将代码转化为架构图</font></font></a></li>
</ul>
</li>
<li><a href="#linux"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Linux</font></font></a>
<ul dir="auto">
<li><a href="#linux-file-system-explained"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Linux 文件系统详解</font></font></a></li>
<li><a href="#18-most-used-linux-commands-you-should-know"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">你应该知道的 18 个最常用的 Linux 命令</font></font></a></li>
</ul>
</li>
<li><a href="#security"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">安全</font></font></a>
<ul dir="auto">
<li><a href="#how-does-https-work"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">HTTPS 如何工作？</font></font></a></li>
<li><a href="#oauth-20-explained-with-simple-terms"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">用简单的术语解释 Oauth 2.0。</font></font></a></li>
<li><a href="#top-4-forms-of-authentication-mechanisms"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">四种主要的身份验证机制</font></font></a></li>
<li><a href="#session-cookie-jwt-token-sso-and-oauth-20---what-are-they"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">会话、cookie、JWT、令牌、SSO 和 OAuth 2.0 - 它们是什么？</font></font></a></li>
<li><a href="#how-to-store-passwords-safely-in-the-database-and-how-to-validate-a-password"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如何在数据库中安全地存储密码以及如何验证密码？</font></font></a></li>
<li><a href="#explaining-json-web-token-jwt-to-a-10-year-old-kid"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">向 10 岁的孩子解释 JSON Web Token (JWT)</font></font></a></li>
<li><a href="#how-does-google-authenticator-or-other-types-of-2-factor-authenticators-work"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Google Authenticator（或其他类型的双因素身份验证器）如何工作？</font></font></a></li>
</ul>
</li>
<li><a href="#real-world-case-studies"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">真实案例研究</font></font></a>
<ul dir="auto">
<li><a href="#netflixs-tech-stack"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Netflix 的技术栈</font></font></a></li>
<li><a href="#twitter-architecture-2022"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Twitter 架构 2022</font></font></a></li>
<li><a href="#evolution-of-airbnbs-microservice-architecture-over-the-past-15-years"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Airbnb 微服务架构 15 年的演进</font></font></a></li>
<li><a href="#monorepo-vs-microrepo"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Monorepo 与 Microrepo。</font></font></a></li>
<li><a href="#how-will-you-design-the-stack-overflow-website"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">您将如何设计 Stack Overflow 网站？</font></font></a></li>
<li><a href="#why-did-amazon-prime-video-monitoring-move-from-serverless-to-monolithic-how-can-it-save-90-cost"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">为什么 Amazon Prime Video 监控从无服务器转向单片机？它如何节省 90% 的成本？</font></font></a></li>
<li><a href="#how-does-disney-hotstar-capture-5-billion-emojis-during-a-tournament"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Disney Hotstar 如何在比赛期间捕捉 50 亿个表情符号？</font></font></a></li>
<li><a href="#how-discord-stores-trillions-of-messages"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Discord 如何存储数万亿条消息</font></font></a></li>
<li><a href="#how-do-video-live-streamings-work-on-youtube-tiktok-live-or-twitch"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">YouTube、TikTok Live 或 Twitch 上的视频直播如何进行？</font></font></a></li>
</ul>
</li>
</ul>

<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">通信协议</font></font></h2><a id="user-content-communication-protocols" class="anchor" aria-label="永久链接：通信协议" href="#communication-protocols"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">架构风格定义了应用程序编程接口 (API) 的不同组件如何相互交互。因此，它们通过提供设计和构建 API 的标准方法来确保效率、可靠性和与其他系统的轻松集成。以下是最常用的风格：</font></font></p>
<p dir="auto">
  <a target="_blank" rel="noopener noreferrer" href="/ByteByteGoHq/system-design-101/blob/main/images/api-architecture-styles.png"><img src="/ByteByteGoHq/system-design-101/raw/main/images/api-architecture-styles.png" style="width: 640px; max-width: 100%;"></a>
</p>
<ul dir="auto">
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">肥皂：&nbsp;</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">成熟、全面、基于 XML</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">最适合企业应用程序&nbsp;</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">REST 风格的：&nbsp;</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">流行且易于实现的 HTTP 方法&nbsp;</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">非常适合 Web 服务&nbsp;</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">GraphQL：&nbsp;</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">查询语言，请求特定数据&nbsp;</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">减少网络开销，加快响应速度&nbsp;</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">gRPC：&nbsp;</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">现代、高性能的协议缓冲区&nbsp;</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">适用于微服务架构&nbsp;</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">WebSocket 接口：&nbsp;</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">实时、双向、持久连接&nbsp;</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">非常适合低延迟数据交换&nbsp;</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Webhook：&nbsp;</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">事件驱动、HTTP 回调、异步&nbsp;</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">当事件发生时通知系统</font></font></p>
</li>
</ul>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">REST API 与 GraphQL</font></font></h3><a id="user-content-rest-api-vs-graphql" class="anchor" aria-label="永久链接：REST API 与 GraphQL" href="#rest-api-vs-graphql"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在 API 设计方面，REST 和 GraphQL 各有优缺点。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">下图显示了 REST 和 GraphQL 之间的快速比较。</font></font></p>
<p dir="auto">
  <a target="_blank" rel="noopener noreferrer" href="/ByteByteGoHq/system-design-101/blob/main/images/graphQL.jpg"><img src="/ByteByteGoHq/system-design-101/raw/main/images/graphQL.jpg" style="max-width: 100%;"></a>
</p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">休息</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用标准 HTTP 方法（如 GET、POST、PUT、DELETE）进行 CRUD 操作。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">当您需要单独的服务/应用程序之间简单、统一的接口时，它可以很好地发挥作用。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">缓存策略很容易实现。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">缺点是可能需要多次往返才能从不同的端点收集相关数据。</font></font></li>
</ul>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">GraphQL</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">为客户提供单一端点，以便其精确查询所需的数据。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">客户端指定嵌套查询中所需的精确字段，服务器返回仅包含这些字段的优化负载。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">支持修改数据的变异和实时通知的订阅。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">非常适合聚合来自多个来源的数据，并且可以很好地满足快速发展的前端需求。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">然而，它将复杂性转移到了客户端，如果没有得到适当的保护，可能会允许滥用查询</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">缓存策略可能比 REST 更复杂。</font></font></li>
</ul>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">REST 和 GraphQL 之间的最佳选择取决于应用程序和开发团队的具体要求。GraphQL 非常适合复杂或频繁变化的前端需求，而 REST 则适合那些喜欢简单且一致的契约的应用程序。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">这两种 API 方法都不是万灵药。仔细评估需求和权衡利弊对于选择正确的样式非常重要。REST 和 GraphQL 都是公开数据和支持现代应用程序的有效选择。</font></font></p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">gRPC 如何工作？</font></font></h3><a id="user-content-how-does-grpc-work" class="anchor" aria-label="永久链接：gRPC 如何工作？" href="#how-does-grpc-work"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">RPC（Remote Procedure Call）之所以被称为“</font></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">远程</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">”，是因为在微服务架构下，当服务部署到不同的服务器时，它能够实现远程服务之间的通信。从用户的角度来看，它就像本地的函数调用。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">下图说明了</font></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">gRPC</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">的整体数据流。</font></font></p>
<p dir="auto">
  <a target="_blank" rel="noopener noreferrer" href="/ByteByteGoHq/system-design-101/blob/main/images/grpc.jpg"><img src="/ByteByteGoHq/system-design-101/raw/main/images/grpc.jpg" style="max-width: 100%;"></a>
</p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">步骤 1：从客户端发出 REST 调用。请求主体通常为 JSON 格式。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">步骤2-4：订单服务（gRPC客户端）接收REST调用，进行转换，然后对支付服务进行RPC调用。gRPC将</font></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">客户端存根编码</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">为二进制格式，并将其发送到低级传输层。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">步骤 5：gRPC 通过 HTTP2 在网络上发送数据包。由于二进制编码和网络优化，gRPC 的速度据说比 JSON 快 5 倍。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">步骤 6 - 8：支付服务（gRPC 服务器）从网络接收数据包、对其进行解码并调用服务器应用程序。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">步骤 9 - 11：结果从服务器应用程序返回，并进行编码并发送到传输层。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">步骤 12 - 14：订单服务接收数据包、解码并将结果发送给客户端应用程序。</font></font></p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">什么是 webhook？</font></font></h3><a id="user-content-what-is-a-webhook" class="anchor" aria-label="永久链接：什么是 webhook？" href="#what-is-a-webhook"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">下图显示了轮询和 Webhook 之间的比较。&nbsp;</font></font></p>
<p dir="auto">
  <a target="_blank" rel="noopener noreferrer" href="/ByteByteGoHq/system-design-101/blob/main/images/webhook.jpeg"><img src="/ByteByteGoHq/system-design-101/raw/main/images/webhook.jpeg" style="width: 680px; max-width: 100%;"></a>
</p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">假设我们经营一个电子商务网站。客户通过 API 网关将订单发送到订单服务，然后发送到支付服务进行支付交易。然后，支付服务与外部支付服务提供商 (PSP) 对话以完成交易。&nbsp;</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">有两种方法可以处理与外部 PSP 的通信。&nbsp;</font></font></p>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">1. 简短轮询</font></font></strong>&nbsp;</p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">支付服务向PSP发送支&ZeroWidthSpace;&ZeroWidthSpace;付请求后，会不断向PSP询问支付状态，经过几轮询问后，PSP最终返回支付状态。&nbsp;</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">短轮询有两个缺点：&nbsp;</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">不断轮询状态需要支付服务的资源。&nbsp;</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">外部服务直接与支付服务通信，产生安全漏洞。&nbsp;</font></font></li>
</ul>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">2. Webhook</font></font></strong>&nbsp;</p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">我们可以向外部服务注册一个 webhook。这意味着：当您对请求有更新时，在某个 URL 上回调我。当 PSP 完成处理后，它将调用 HTTP 请求来更新支付状态。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">这样，编程范式就发生了改变，支付服务不再需要浪费资源去轮询支付状态。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如果 PSP 一直不回电怎么办？我们可以设置一个日常工作来每小时检查一次付款状态。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Webhook 通常被称为反向 API 或推送 API，因为服务器会向客户端发送 HTTP 请求。使用 Webhook 时我们需要注意 3 件事：</font></font></p>
<ol dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">我们需要设计一个合适的API供外部服务调用。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">出于安全原因，我们需要在 API 网关中设置适当的规则。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">我们需要在外部服务上注册正确的URL。</font></font></li>
</ol>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如何提高API性能？</font></font></h3><a id="user-content-how-to-improve-api-performance" class="anchor" aria-label="永久链接：如何提高 API 性能？" href="#how-to-improve-api-performance"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">下图展示了提高 API 性能的 5 个常见技巧。</font></font></p>
<p dir="auto">
  <a target="_blank" rel="noopener noreferrer" href="/ByteByteGoHq/system-design-101/blob/main/images/api-performance.jpg"><img src="/ByteByteGoHq/system-design-101/raw/main/images/api-performance.jpg" style="max-width: 100%;"></a>
</p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">分页</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">这是结果较大时常见的优化，将结果流回客户端，以提高服务响应能力。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">异步日志记录</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">同步日志记录每次调用都会处理磁盘，这会降低系统速度。异步日志记录首先将日志发送到无锁缓冲区并立即返回。日志将定期刷新到磁盘。这大大减少了 I/O 开销。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">缓存</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">我们可以将经常访问的数据存入缓存。客户端可以先查询缓存，而不是直接访问数据库。如果发生缓存未命中，客户端可以从数据库查询。像 Redis 这样的缓存将数据存储在内存中，因此数据访问速度比数据库快得多。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">有效载荷压缩</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">请求和响应可以使用 gzip 等进行压缩，以便传输的数据大小更小。这加快了上传和下载速度。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">连接池</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">访问资源时，我们经常需要从数据库加载数据。打开和关闭数据库连接会增加很大的开销。因此，我们应该通过打开的连接池连接到数据库。连接池负责管理连接生命周期。</font></font></p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">HTTP 1.0 -&gt; HTTP 1.1 -&gt; HTTP 2.0 -&gt; HTTP 3.0（QUIC）</font></font></h3><a id="user-content-http-10---http-11---http-20---http-30-quic" class="anchor" aria-label="永久链接：HTTP 1.0 -> HTTP 1.1 -> HTTP 2.0 -> HTTP 3.0 (QUIC)" href="#http-10---http-11---http-20---http-30-quic"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">每一代HTTP都解决了什么问题？</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">下图说明了其主要特征。</font></font></p>
<p dir="auto">
  <a target="_blank" rel="noopener noreferrer" href="/ByteByteGoHq/system-design-101/blob/main/images/http3.jpg"><img src="/ByteByteGoHq/system-design-101/raw/main/images/http3.jpg" style="max-width: 100%;"></a>
</p>
<ul dir="auto">
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">HTTP 1.0 于 1996 年完成并有完整的文档。对同一服务器的每个请求都需要单独的 TCP 连接。</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">HTTP 1.1 于 1997 年发布。TCP 连接可以保持开放以供重用（持久连接），但它不能解决 HOL（队头）阻塞问题。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">HOL 阻塞 - 当浏览器允许的并行请求数用完时，后续请求需要等待前一个请求完成。</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">HTTP 2.0于2015年发布，通过请求多路复用来解决HOL问题，消除了应用层的HOL阻塞，但是传输（TCP）层的HOL仍然存在。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如图所示，HTTP 2.0 引入了 HTTP“流”的概念：这是一种抽象概念，允许将不同的 HTTP 交换复用到同一个 TCP 连接上。每个流不需要按顺序发送。</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">HTTP 3.0 初稿于 2020 年发布。它是 HTTP 2.0 的拟议继任者。它使用 QUIC 而不是 TCP 作为底层传输协议，从而消除了传输层的 HOL 阻塞。</font></font></p>
</li>
</ul>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">QUIC 基于 UDP。它将流作为传输层的一等公民引入。QUIC 流共享同一个 QUIC 连接，因此无需额外的握手和慢启动即可创建新的流，但 QUIC 流是独立传送的，因此在大多数情况下，影响一个流的数据包丢失不会影响其他流。</font></font></p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">SOAP、REST、GraphQL、RPC 的比较</font></font></h3><a id="user-content-soap-vs-rest-vs-graphql-vs-rpc" class="anchor" aria-label="永久链接：SOAP、REST、GraphQL 和 RPC" href="#soap-vs-rest-vs-graphql-vs-rpc"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">下图说明了 API 时间线和 API 样式比较。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">随着时间的推移，不同的 API 架构风格相继发布。每种架构风格都有自己的标准化数据交换模式。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">您可以在图表中查看每种风格的用例。</font></font></p>
<p dir="auto">
  <a target="_blank" rel="noopener noreferrer" href="/ByteByteGoHq/system-design-101/blob/main/images/SOAP vs REST vs GraphQL vs RPC.jpeg"><img src="/ByteByteGoHq/system-design-101/raw/main/images/SOAP vs REST vs GraphQL vs RPC.jpeg" style="max-width: 100%;"></a>
</p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码优先与 API 优先</font></font></h3><a id="user-content-code-first-vs-api-first" class="anchor" aria-label="永久链接：代码优先与 API 优先" href="#code-first-vs-api-first"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">下图展示了代码优先开发和 API 优先开发的区别。为什么我们要考虑 API 优先设计？</font></font></p>
<p dir="auto">
  <a target="_blank" rel="noopener noreferrer" href="/ByteByteGoHq/system-design-101/blob/main/images/api_first.jpg"><img src="/ByteByteGoHq/system-design-101/raw/main/images/api_first.jpg" style="width: 680px; max-width: 100%;"></a>
</p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">微服务增加了系统复杂性，我们有单独的服务来提供系统的不同功能。虽然这种架构有利于解耦和职责分离，但我们需要处理服务之间的各种通信。</font></font></li>
</ul>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">最好在编写代码之前仔细考虑系统的复杂性并仔细定义服务的边界。</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">独立的职能团队需要使用相同的语言，而专门的职能团队只负责他们自己的组件和服务。建议组织通过 API 设计使用相同的语言。</font></font></li>
</ul>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在编写代码之前，我们可以模拟请求和响应来验证 API 设计。</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">提高软件质量和开发人员的工作效率由于我们在项目启动时已经解决了大部分不确定因素，因此整体开发过程更加顺畅，软件质量也得到了很大的提高。</font></font></li>
</ul>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">开发人员对这个过程也感到满意，因为他们可以专注于功能开发，而不是处理突然的变化。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">项目生命周期结束时出现意外的可能性减少了。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">因为我们先设计了 API，所以可以在开发代码的同时设计测试。从某种意义上说，使用 API 优先开发时，我们也有 TDD（测试驱动设计）。</font></font></p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">HTTP 状态代码</font></font></h3><a id="user-content-http-status-codes" class="anchor" aria-label="永久链接：HTTP 状态代码" href="#http-status-codes"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto">
  <a target="_blank" rel="noopener noreferrer" href="/ByteByteGoHq/system-design-101/blob/main/images/http-status-code.jpg"><img src="/ByteByteGoHq/system-design-101/raw/main/images/http-status-code.jpg" style="width: 540px; max-width: 100%;"></a>
</p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">HTTP 的响应代码分为五类：</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">信息 (100-199) 成功 (200-299) 重定向 (300-399) 客户端错误 (400-499) 服务器错误 (500-599)</font></font></p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">API 网关起什么作用？</font></font></h3><a id="user-content-what-does-api-gateway-do" class="anchor" aria-label="永久链接：API网关起什么作用？" href="#what-does-api-gateway-do"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">下图显示了详细信息。</font></font></p>
<p dir="auto">
  <a target="_blank" rel="noopener noreferrer" href="/ByteByteGoHq/system-design-101/blob/main/images/api_gateway.jpg"><img src="/ByteByteGoHq/system-design-101/raw/main/images/api_gateway.jpg" style="width: 520px; max-width: 100%;"></a>
</p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">步骤 1-客户端向 API 网关发送 HTTP 请求。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">第 2 步 - API 网关解析并验证 HTTP 请求中的属性。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">步骤 3——API 网关执行允许列表/拒绝列表检查。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">步骤 4-API 网关与身份提供商对话以进行身份&ZeroWidthSpace;&ZeroWidthSpace;验证和授权。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">步骤 5 - 速率限制规则应用于请求。如果超出限制，则请求被拒绝。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">第 6 步和第 7 步 - 现在请求已经通过了基本检查，API 网关通过路径匹配找到要路由到的相关服务。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">步骤8-API网关将请求转换为适当的协议并将其发送到后端微服务。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">步骤 9-12：API 网关可以正确处理错误，如果错误需要较长时间才能恢复（断路），则处理故障。它还可以利用 ELK（Elastic-Logstash-Kibana）堆栈进行日志记录和监控。我们有时会在 API 网关中缓存数据。</font></font></p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">我们如何设计有效且安全的 API？</font></font></h3><a id="user-content-how-do-we-design-effective-and-safe-apis" class="anchor" aria-label="永久链接：我们如何设计有效且安全的 API？" href="#how-do-we-design-effective-and-safe-apis"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">下图以购物车示例展示了典型的 API 设计。</font></font></p>
<p dir="auto">
  <a target="_blank" rel="noopener noreferrer" href="/ByteByteGoHq/system-design-101/blob/main/images/safe-apis.jpg"><img src="/ByteByteGoHq/system-design-101/raw/main/images/safe-apis.jpg" style="max-width: 100%;"></a>
</p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">请注意，API 设计不仅仅是 URL 路径设计。大多数时候，我们需要选择适当的资源名称、标识符和路径模式。设计适当的 HTTP 标头字段或在 API 网关内设计有效的速率限制规则也同样重要。</font></font></p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">TCP/IP 封装</font></font></h3><a id="user-content-tcpip-encapsulation" class="anchor" aria-label="永久链接：TCP/IP 封装" href="#tcpip-encapsulation"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">数据是如何通过网络发送的？为什么 OSI 模型需要这么多层？</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">下图显示了数据在网络传输时如何封装和解封装。</font></font></p>
<p dir="auto">
  <a target="_blank" rel="noopener noreferrer" href="/ByteByteGoHq/system-design-101/blob/main/images/osi model.jpeg"><img src="/ByteByteGoHq/system-design-101/raw/main/images/osi model.jpeg" style="max-width: 100%;"></a>
</p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">步骤1：当设备A通过HTTP协议通过网络向设备B发送数据时，首先在应用层添加HTTP报头。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">步骤 2：然后为数据添加 TCP 或 UDP 报头。它在传输层封装成 TCP 段。报头包含源端口、目标端口和序列号。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">步骤 3：然后在网络层用 IP 报头封装这些段。IP 报头包含源/目标 IP 地址。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">步骤4：IP数据报在数据链路层添加MAC头，其中包含源/目标MAC地址。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">步骤5：封装的帧被发送到物理层并以二进制位的形式通过网络发送。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">步骤 6-10：当设备 B 从网络收到数据时，它会执行解封装过程，这是封装过程的逆过程。逐层删除报头，最终设备 B 可以读取数据。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">网络模型需要分层，因为每一层都专注于自己的职责。每一层都可以依靠报头来处理指令，而不需要知道上一层数据的含义。</font></font></p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">为什么 Nginx 被称为“反向”代理？</font></font></h3><a id="user-content-why-is-nginx-called-a-reverse-proxy" class="anchor" aria-label="永久链接：为什么 Nginx 被称为“反向”代理？" href="#why-is-nginx-called-a-reverse-proxy"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">下图显示了 𝐟𝐨𝐫𝐰𝐚𝐫𝐝 𝐩𝐫𝐨𝐱𝐲 和 𝐫𝐞𝐯𝐞𝐫𝐬𝐞 𝐩𝐫𝐨𝐱𝐲 之间的区别。</font></font></p>
<p dir="auto">
  <a target="_blank" rel="noopener noreferrer" href="/ByteByteGoHq/system-design-101/blob/main/images/Forward Proxy v.s. Reverse Proxy2x.jpg"><img src="/ByteByteGoHq/system-design-101/raw/main/images/Forward Proxy v.s. Reverse Proxy2x.jpg" style="width: 720px; max-width: 100%;"></a>
</p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">正向代理是位于用户设备和互联网之间的服务器。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">正向代理通常用于：</font></font></p>
<ol dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">保护客户</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">绕过浏览限制</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">阻止访问某些内容</font></font></li>
</ol>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">反向代理是一种服务器，它接受来自客户端的请求，将请求转发到 Web 服务器，并将结果返回给客户端，就好像代理服务器已处理该请求一样。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">反向代理适用于：</font></font></p>
<ol dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">保护服务器</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">负载均衡</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">缓存静态内容</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">加密和解密 SSL 通信</font></font></li>
</ol>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">常见的负载均衡算法有哪些？</font></font></h3><a id="user-content-what-are-the-common-load-balancing-algorithms" class="anchor" aria-label="永久链接：有哪些常见的负载平衡算法？" href="#what-are-the-common-load-balancing-algorithms"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">下图展示了6种常见的算法。</font></font></p>
<p dir="auto">
  <a target="_blank" rel="noopener noreferrer" href="/ByteByteGoHq/system-design-101/blob/main/images/lb-algorithms.jpg"><img src="/ByteByteGoHq/system-design-101/raw/main/images/lb-algorithms.jpg" style="max-width: 100%;"></a>
</p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">静态算法</font></font></li>
</ul>
<ol dir="auto">
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">循环赛</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">客户端请求按顺序发送到不同的服务实例。服务通常要求是无状态的。</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">粘性循环</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">这是对循环算法的改进。如果 Alice 的第一个请求转到服务 A，则后续请求也会转到服务 A。</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">加权循环赛</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">管理员可以指定每个服务的权重。权重较高的服务比其他服务处理更多的请求。</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">哈希</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">该算法对传入请求的 IP 或 URL 应用哈希函数。根据哈希函数结果将请求路由到相关实例。</font></font></p>
</li>
</ol>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">动态算法</font></font></li>
</ul>
<ol start="5" dir="auto">
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">最少连接</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">新的请求被发送到具有最少并发连接的服务实例。</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">最短响应时间</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">新的请求被发送到响应时间最快的服务实例。</font></font></p>
</li>
</ol>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">URL、URI、URN——您知道它们之间的区别吗？</font></font></h3><a id="user-content-url-uri-urn---do-you-know-the-differences" class="anchor" aria-label="永久链接：URL、URI、URN——您知道它们之间的区别吗？" href="#url-uri-urn---do-you-know-the-differences"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">下图显示了 URL、URI 和 URN 的比较。</font></font></p>
<p dir="auto">
  <a target="_blank" rel="noopener noreferrer" href="/ByteByteGoHq/system-design-101/blob/main/images/url-uri-urn.jpg"><img src="/ByteByteGoHq/system-design-101/raw/main/images/url-uri-urn.jpg" style="max-width: 100%;"></a>
</p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">URI</font></font></li>
</ul>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">URI 代表统一资源标识符。它标识网络上的逻辑或物理资源。URL 和 URN 是 URI 的子类型。URL 定位资源，而 URN 命名资源。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">URI 由以下部分组成：scheme:[//authority]path[?query][#fragment]</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">网址</font></font></li>
</ul>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">URL 代表统一资源定位符，是 HTTP 的关键概念。它是网络上唯一资源的地址。它可以与其他协议（如 FTP 和 JDBC）一起使用。</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">瓮</font></font></li>
</ul>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">URN 代表统一资源名称。它使用 urn 方案。URN 不能用于定位资源。图中给出的一个简单示例由命名空间和特定于命名空间的字符串组成。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如果您想了解有关该主题的更多详细信息，我建议您查看</font></font><a href="https://www.w3.org/TR/uri-clarification/" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">W3C 的澄清</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。</font></font></p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">持续集成/持续交付</font></font></h2><a id="user-content-cicd" class="anchor" aria-label="固定链接: CI/CD" href="#cicd"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">简单解释 CI/CD 管道</font></font></h3><a id="user-content-cicd-pipeline-explained-in-simple-terms" class="anchor" aria-label="永久链接：简单解释 CI/CD 管道" href="#cicd-pipeline-explained-in-simple-terms"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto">
  <a target="_blank" rel="noopener noreferrer" href="/ByteByteGoHq/system-design-101/blob/main/images/ci-cd-pipeline.jpg"><img src="/ByteByteGoHq/system-design-101/raw/main/images/ci-cd-pipeline.jpg" style="width: 680px; max-width: 100%;"></a>
</p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">第 1 部分 - 使用 CI/CD 的 SDLC</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">软件开发生命周期 (SDLC) 包括几个关键阶段：开发、测试、部署和维护。CI/CD 可自动化和集成这些阶段，以实现更快、更可靠的发布。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">当代码被推送到 git 存储库时，它会触发自动构建和测试过程。运行端到端 (e2e) 测试用例来验证代码。如果测试通过，代码可以自动部署到暂存/生产。如果发现问题，代码将被发回开发部门进行错误修复。这种自动化为开发人员提供快速反馈，并降低生产中出现错误的风险。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">第 2 节 - CI 和 CD 之间的区别</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">持续集成 (CI) 可自动执行构建、测试和合并过程。每当提交代码时，它都会运行测试，以便尽早发现集成问题。这鼓励频繁提交代码并快速反馈。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">持续交付 (CD) 可自动化发布流程，例如基础设施变更和部署。它确保软件可以通过自动化工作流程随时可靠地发布。CD 还可以自动化生产部署前所需的手动测试和批准步骤。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">第 3 部分 - CI/CD 管道</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">典型的 CI/CD 管道有几个连接阶段：</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">开发人员将代码更改提交到源代码管理</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">CI 服务器检测到更改并触发构建</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代码已编译并测试（单元测试、集成测试）</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">测试结果报告给开发人员</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">成功后，工件将部署到暂存环境</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">发布前可能会进行进一步的测试</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">CD 系统将批准的变更部署到生产中</font></font></li>
</ul>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Netflix 技术栈（CI/CD 管道）</font></font></h3><a id="user-content-netflix-tech-stack-cicd-pipeline" class="anchor" aria-label="永久链接：Netflix 技术栈（CI/CD 管道）" href="#netflix-tech-stack-cicd-pipeline"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto">
  <a target="_blank" rel="noopener noreferrer" href="/ByteByteGoHq/system-design-101/blob/main/images/netflix-ci-cd.jpg"><img src="/ByteByteGoHq/system-design-101/raw/main/images/netflix-ci-cd.jpg" style="width: 720px; max-width: 100%;"></a>
</p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">规划：Netflix Engineering 使用 JIRA 进行规划，使用 Confluence 进行文档编制。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">编码：Java 是后端服务的主要编程语言，而其他语言则用于不同的用例。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">构建：Gradle 主要用于构建，并且构建了 Gradle 插件来支持各种用例。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">打包：将包和依赖项打包到 Amazon Machine Image (AMI) 中以供发布。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">测试：测试强调生产文化对构建混沌工具的关注。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">部署：Netflix采用自建的Spinnaker进行金丝雀发布部署。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">监控：监控指标集中在 Atlas，并使用 Kayenta 检测异常。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">事件上报：按照优先级别调度事件，使用PagerDuty进行事件处理。</font></font></p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">架构模式</font></font></h2><a id="user-content-architecture-patterns" class="anchor" aria-label="永久链接：架构模式" href="#architecture-patterns"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MVC、MVP、MVVM、MVVM-C 和 VIPER</font></font></h3><a id="user-content-mvc-mvp-mvvm-mvvm-c-and-viper" class="anchor" aria-label="永久链接：MVC、MVP、MVVM、MVVM-C 和 VIPER" href="#mvc-mvp-mvvm-mvvm-c-and-viper"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">这些架构模式是应用开发中最常用的模式之一，无论是在 iOS 还是 Android 平台上。开发人员引入这些模式是为了克服早期模式的局限性。那么，它们有何不同？</font></font></p>
<p dir="auto">
  <a target="_blank" rel="noopener noreferrer" href="/ByteByteGoHq/system-design-101/blob/main/images/client arch patterns.png"><img src="/ByteByteGoHq/system-design-101/raw/main/images/client arch patterns.png" style="width: 720px; max-width: 100%;"></a>
</p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MVC 是最古老的模式，已有近 50 年历史</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">每个模式都有一个“视图”（V），负责显示内容和接收用户输入</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">大多数模式都包含一个“模型”（M）来管理业务数据</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">“控制器”、“演示者”和“视图模型”是视图和模型（VIPER 模式中的“实体”）之间的中介</font></font></li>
</ul>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">每个开发人员都应该知道的 18 个关键设计模式</font></font></h3><a id="user-content-18-key-design-patterns-every-developer-should-know" class="anchor" aria-label="永久链接：每个开发人员都应该知道的 18 个关键设计模式" href="#18-key-design-patterns-every-developer-should-know"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">模式是常见设计问题的可重复使用解决方案，可使开发过程更顺畅、更高效。它们是构建更好软件结构的蓝图。以下是一些最流行的模式：</font></font></p>
<p dir="auto">
  <a target="_blank" rel="noopener noreferrer" href="/ByteByteGoHq/system-design-101/blob/main/images/18-oo-patterns.png"><img src="/ByteByteGoHq/system-design-101/raw/main/images/18-oo-patterns.png" style="max-width: 100%;"></a>
</p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">抽象工厂：家庭创造者——将相关物品分组。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">建造者：乐高大师——一步步建造物体，将创造与外观分开。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">原型：克隆制作器——创建已准备好的示例的副本。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">单例：唯一 - 只有一个实例的特殊类。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">适配器：通用插头——连接具有不同接口的东西。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">桥梁：功能连接器 - 将对象的工作方式与其功能联系起来。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">复合：树生成器——形成由简单和复杂部分组成的树状结构。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">装饰器：定制器——在不改变对象核心的情况下为其添加功能。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">外观：一站式服务 - 用单一、简化的界面来代表整个系统。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Flyweight：节省空间 - 高效共享小型、可重复使用的物品。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">代理：替身演员 - 代表另一个对象，控制访问或操作。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">责任链：请求中继 - 通过对象链传递请求直至得到处理。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">命令：任务包装器 - 将请求转换为对象，准备采取行动。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">迭代器：集合资源管理器 - 逐个访问集合中的元素。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">中介：通讯中心——简化不同类别之间的交互。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">纪念品：时间胶囊——捕获并恢复物体的状态。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">观察者：新闻广播员——向类别通报其他对象发生的变化。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">访客：熟练的客人 - 在不改变类的情况下向其添加新的操作。</font></font></li>
</ul>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">数据库</font></font></h2><a id="user-content-database" class="anchor" aria-label="永久链接：数据库" href="#database"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">一份关于云服务中不同数据库的详细清单</font></font></h3><a id="user-content-a-nice-cheat-sheet-of-different-databases-in-cloud-services" class="anchor" aria-label="永久链接：云服务中不同数据库的简要介绍" href="#a-nice-cheat-sheet-of-different-databases-in-cloud-services"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto">
  <a target="_blank" rel="noopener noreferrer" href="/ByteByteGoHq/system-design-101/blob/main/images/cloud-dbs2.png"><img src="/ByteByteGoHq/system-design-101/raw/main/images/cloud-dbs2.png" style="max-width: 100%;"></a>
</p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">为项目选择合适的数据库是一项复杂的任务。许多数据库选项都适用于不同的用例，这很快就会导致决策疲劳。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">我们希望这份备忘单能够提供高层的指导，帮助您找到符合您项目需求的正确服务并避免潜在的陷阱。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">注意：Google 的数据库使用案例文档有限。尽管我们尽了最大努力查看可用内容并得出最佳选择，但某些条目可能需要更准确。</font></font></p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">为数据库提供动力的 8 种数据结构</font></font></h3><a id="user-content-8-data-structures-that-power-your-databases" class="anchor" aria-label="永久链接：为数据库提供支持的 8 种数据结构" href="#8-data-structures-that-power-your-databases"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">答案将因您的用例而异。数据可以在内存或磁盘上建立索引。同样，数据格式也各不相同，例如数字、字符串、地理坐标等。系统可能是写入密集型或读取密集型。所有这些因素都会影响您对数据库索引格式的选择。</font></font></p>
<p dir="auto">
  <a target="_blank" rel="noopener noreferrer" href="/ByteByteGoHq/system-design-101/blob/main/images/8-ds-db.jpg"><img src="/ByteByteGoHq/system-design-101/raw/main/images/8-ds-db.jpg" style="max-width: 100%;"></a>
</p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">以下是用于索引数据的一些最流行的数据结构：</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Skiplist：一种常见的内存索引类型。用于 Redis</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">哈希索引：“Map”数据结构（或“Collection”）的一种非常常见的实现</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">SSTable：不可变的磁盘“Map”实现</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LSM 树：Skiplist + SSTable。高写入吞吐量</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">B-tree：基于磁盘的解决方案。一致的读写性能</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">倒排索引：用于文档索引。在Lucene中使用</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">后缀树：用于字符串模式搜索</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">R树：多维搜索，例如查找最近邻居</font></font></li>
</ul>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">SQL语句在数据库中是如何执行的？</font></font></h3><a id="user-content-how-is-an-sql-statement-executed-in-the-database" class="anchor" aria-label="永久链接：SQL 语句在数据库中是如何执行的？" href="#how-is-an-sql-statement-executed-in-the-database"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">下图展示了这个过程。请注意，不同数据库的架构不同，该图演示了一些常见的设计。</font></font></p>
<p dir="auto">
  <a target="_blank" rel="noopener noreferrer" href="/ByteByteGoHq/system-design-101/blob/main/images/sql execution order in db.jpeg"><img src="/ByteByteGoHq/system-design-101/raw/main/images/sql execution order in db.jpeg" style="width: 580px; max-width: 100%;"></a>
</p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">步骤 1-通过传输层协议（例如TCP）将SQL语句发送到数据库。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">第 2 步 - SQL 语句被发送到命令解析器，在那里它经过句法和语义分析，然后生成查询树。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">步骤 3 - 查询树被发送给优化器。优化器创建执行计划。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">步骤 4 - 执行计划发送给执行器。执行器从执行中检索数据。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">步骤 5-访问方法提供执行所需的数据获取逻辑，从存储引擎检索数据。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">第 6 步 - 访问方法决定 SQL 语句是否为只读。如果查询是只读的（SELECT 语句），则将其传递给缓冲区管理器进行进一步处理。缓冲区管理器在缓存或数据文件中查找数据。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">步骤 7 - 如果该语句是 UPDATE 或 INSERT，则将其传递给事务管理器进行进一步处理。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">步骤 8 - 在事务期间，数据处于锁定模式。这是由锁管理器保证的。它还确保了事务的 ACID 属性。</font></font></p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">CAP 定理</font></font></h3><a id="user-content-cap-theorem" class="anchor" aria-label="永久链接：CAP 定理" href="#cap-theorem"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">CAP 定理是计算机科学中最著名的术语之一，但我敢打赌不同的开发人员对此有不同的理解。让我们来看看它是什么，以及为什么它会让人困惑。</font></font></p>
<p dir="auto">
  <a target="_blank" rel="noopener noreferrer" href="/ByteByteGoHq/system-design-101/blob/main/images/cap theorem.jpeg"><img src="/ByteByteGoHq/system-design-101/raw/main/images/cap theorem.jpeg" style="max-width: 100%;"></a>
</p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">CAP 定理指出分布式系统不能同时提供这三个保证中的两个以上。</font></font></p>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">一致性</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：一致性意味着无论所有客户端连接到哪个节点，它们都会同时看到相同的数据。</font></font></p>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">可用性</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：可用性意味着即使某些节点出现故障，任何请求数据的客户端都会得到响应。</font></font></p>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">分区容忍度</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：分区表示两&ZeroWidthSpace;&ZeroWidthSpace;个节点之间的通信中断。分区容忍度意味着尽管出现网络分区，系统仍可继续运行。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">“2 之 3” 公式可能有用，</font></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">但这种简化可能会产生误导</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。</font></font></p>
<ol dir="auto">
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">选择数据库并不容易。仅基于 CAP 定理来证明我们的选择是不够的。例如，公司不会仅仅因为 Cassandra 是一个 AP 系统而选择它用于聊天应用程序。Cassandra 具有一系列优良特性，是存储聊天消息的理想选择。我们需要深入挖掘。</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">“CAP 仅禁止了设计空间的一小部分：在分区存在的情况下实现完美的可用性和一致性，这种情况很少见”。引自论文：CAP 十二年后：“规则”如何改变。</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">该定理是关于 100% 可用性和一致性的。更现实的讨论是在没有网络分区的情况下，延迟和一致性之间的权衡。有关更多详细信息，请参阅 PACELC 定理。</font></font></p>
</li>
</ol>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">CAP 定理真的有用吗？</font></font></strong></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">我认为它仍然很有用，因为它让我们对一系列权衡讨论有了更多的了解，但这只是故事的一部分。在选择正确的数据库时，我们需要更深入地挖掘。</font></font></p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">内存和存储的类型</font></font></h3><a id="user-content-types-of-memory-and-storage" class="anchor" aria-label="永久链接：内存和存储的类型" href="#types-of-memory-and-storage"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto">
  <a target="_blank" rel="noopener noreferrer" href="/ByteByteGoHq/system-design-101/blob/main/images/Types_of_Memory_and_Storage.jpeg"><img src="/ByteByteGoHq/system-design-101/raw/main/images/Types_of_Memory_and_Storage.jpeg" style="width: 420px; max-width: 100%;"></a>
</p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">可视化 SQL 查询</font></font></h3><a id="user-content-visualizing-a-sql-query" class="anchor" aria-label="永久链接：可视化 SQL 查询" href="#visualizing-a-sql-query"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto">
  <a target="_blank" rel="noopener noreferrer" href="/ByteByteGoHq/system-design-101/blob/main/images/sql-execution-order.jpg"><img src="/ByteByteGoHq/system-design-101/raw/main/images/sql-execution-order.jpg" style="width: 580px; max-width: 100%;"></a>
</p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">数据库系统分几个步骤执行SQL语句，包括：</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">解析 SQL 语句并检查其有效性</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">将 SQL 转换为内部表示，例如关系代数</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">优化内部表示并创建利用索引信息的执行计划</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">执行计划并返回结果</font></font></li>
</ul>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">SQL的执行非常复杂，涉及许多注意事项，例如：</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">索引和缓存的使用</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">表连接的顺序</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">并发控制</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">交易管理</font></font></li>
</ul>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">SQL 语言</font></font></h3><a id="user-content-sql-language" class="anchor" aria-label="永久链接：SQL 语言" href="#sql-language"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">1986 年，SQL（结构化查询语言）成为标准。在接下来的 40 年里，它成为关系数据库管理系统的主导语言。阅读最新标准（ANSI SQL 2016）可能很耗时。我该如何学习它？</font></font></p>
<p dir="auto">
  <a target="_blank" rel="noopener noreferrer" href="/ByteByteGoHq/system-design-101/blob/main/images/how-to-learn-sql.jpg"><img src="/ByteByteGoHq/system-design-101/raw/main/images/how-to-learn-sql.jpg" style="max-width: 100%;"></a>
</p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">SQL 语言有 5 个组成部分：</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">DDL：数据定义语言，例如 CREATE、ALTER、DROP</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">DQL：数据查询语言，例如 SELECT</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">DML：数据操作语言，例如 INSERT、UPDATE、DELETE</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">DCL：数据控制语言，例如GRANT、REVOKE</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">TCL：事务控制语言，例如COMMIT、ROLLBACK</font></font></li>
</ul>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">对于后端工程师来说，你可能需要了解大部分内容。作为数据分析师，你可能需要对 DQL 有很好的理解。选择与你最相关的主题。</font></font></p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">缓存</font></font></h2><a id="user-content-cache" class="anchor" aria-label="永久链接：缓存" href="#cache"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">数据缓存在各处</font></font></h3><a id="user-content-data-is-cached-everywhere" class="anchor" aria-label="永久链接：数据缓存在各处" href="#data-is-cached-everywhere"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">该图说明了我们在典型架构中缓存数据的位置。</font></font></p>
<p dir="auto">
  <a target="_blank" rel="noopener noreferrer" href="/ByteByteGoHq/system-design-101/blob/main/images/where do we cache data.jpeg"><img src="/ByteByteGoHq/system-design-101/raw/main/images/where do we cache data.jpeg" style="width: 720px; max-width: 100%;"></a>
</p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">沿着流动方向</font><font style="vertical-align: inherit;">有</font></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">多个层。</font></font></strong><font style="vertical-align: inherit;"></font></p>
<ol dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">客户端应用程序：HTTP 响应可以由浏览器缓存。我们第一次通过 HTTP 请求数据，它会在 HTTP 标头中返回过期策略；我们再次请求数据，客户端应用程序会首先尝试从浏览器缓存中检索数据。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">CDN：CDN 缓存静态网络资源。客户端可以从附近的 CDN 节点获取数据。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">负载均衡器：负载均衡器也可以缓存资源。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">消息传递基础设施：消息代理首先将消息存储在磁盘上，然后消费者按照自己的节奏检索它们。根据保留策略，数据会在 Kafka 集群中缓存一段时间。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">服务：服务中有多层缓存。如果数据未缓存在 CPU 缓存中，服务将尝试从内存中检索数据。有时服务有二级缓存，用于将数据存储在磁盘上。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">分布式缓存：像 Redis 这样的分布式缓存在内存中保存多个服务的键值对。它提供比数据库更好的读写性能。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">全文搜索：我们有时需要使用全文搜索，例如 Elastic Search 进行文档搜索或日志搜索。数据副本也会被编入搜索引擎的索引中。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">数据库：即使在数据库中，我们也有不同级别的缓存：</font></font></li>
</ol>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">WAL(Write-ahead Log)：在建立B树索引之前，先将数据写入WAL</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">缓冲池：分配用于缓存查询结果的内存区域</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">物化视图：预先计算查询结果并将其存储在数据库表中，以获得更好的查询性能</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">交易日志：记录所有交易和数据库更新</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">复制日志：用于记录数据库集群中的复制状态</font></font></li>
</ul>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Redis 为何这么快？</font></font></h3><a id="user-content-why-is-redis-so-fast" class="anchor" aria-label="永久链接：Redis 为何这么快？" href="#why-is-redis-so-fast"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如下图所示，主要有 3 个原因。</font></font></p>
<p dir="auto">
  <a target="_blank" rel="noopener noreferrer" href="/ByteByteGoHq/system-design-101/blob/main/images/why_redis_fast.jpeg"><img src="/ByteByteGoHq/system-design-101/raw/main/images/why_redis_fast.jpeg" style="max-width: 100%;"></a>
</p>
<ol dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Redis 是一个基于 RAM 的数据存储。RAM 访问比随机磁盘访问快至少 1000 倍。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Redis 利用 IO 多路复用和单线程执行循环来提高执行效率。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Redis 利用了几种高效的低级数据结构。</font></font></li>
</ol>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">问题：另一种流行的内存存储是 Memcached。您知道 Redis 和 Memcached 之间的区别吗？</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">您可能已经注意到此图的样式与我之前的帖子不同。请告诉我您更喜欢哪一个。</font></font></p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Redis 如何使用？</font></font></h3><a id="user-content-how-can-redis-be-used" class="anchor" aria-label="永久链接：如何使用 Redis？" href="#how-can-redis-be-used"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto">
  <a target="_blank" rel="noopener noreferrer" href="/ByteByteGoHq/system-design-101/blob/main/images/top-redis-use-cases.jpg"><img src="/ByteByteGoHq/system-design-101/raw/main/images/top-redis-use-cases.jpg" style="width: 520px; max-width: 100%;"></a>
</p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Redis 不仅仅只是缓存。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Redis 可用于多种场景，如图所示。</font></font></p>
<ul dir="auto">
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">会议</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">我们可以使用Redis在不同的服务之间共享用户会话数据。</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">缓存</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">我们可以使用Redis来缓存对象或页面，特别是对于热点数据。</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">分布式锁</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">我们可以使用Redis字符串来获取分布式服务之间的锁。</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">柜台</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">我们可以计算文章的点赞数或阅读数。</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">速率限制器</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">我们可以对某些用户 IP 应用速率限制器。</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">全局 ID 生成器</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">我们可以使用 Redis Int 作为全局 ID。</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">购物车</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">我们可以使用 Redis Hash 来表示购物车中的键值对。</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">计算用户留存率</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">我们可以用Bitmap来表示用户每日登录情况，并计算用户留存。</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">消息队列</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">我们可以使用List作为消息队列。</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">排行</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">我们可以使用ZSet对文章进行排序。</font></font></p>
</li>
</ul>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">顶级缓存策略</font></font></h3><a id="user-content-top-caching-strategies" class="anchor" aria-label="永久链接：顶级缓存策略" href="#top-caching-strategies"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">设计大型系统通常需要仔细考虑缓存。以下是经常使用的五种缓存策略。</font></font></p>
<p dir="auto">
  <a target="_blank" rel="noopener noreferrer" href="/ByteByteGoHq/system-design-101/blob/main/images/top_caching_strategy.jpeg"><img src="/ByteByteGoHq/system-design-101/raw/main/images/top_caching_strategy.jpeg" style="width: 680px; max-width: 100%;"></a>
</p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">微服务架构</font></font></h2><a id="user-content-microservice-architecture" class="anchor" aria-label="永久链接：微服务架构" href="#microservice-architecture"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">典型的微服务架构是什么样的？</font></font></h3><a id="user-content-what-does-a-typical-microservice-architecture-look-like" class="anchor" aria-label="永久链接：典型的微服务架构是什么样的？" href="#what-does-a-typical-microservice-architecture-look-like"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto">
  <a target="_blank" rel="noopener noreferrer" href="/ByteByteGoHq/system-design-101/blob/main/images/typical-microservice-arch.jpg"><img src="/ByteByteGoHq/system-design-101/raw/main/images/typical-microservice-arch.jpg" style="width: 520px; max-width: 100%;"></a>
</p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">下图展示了典型的微服务架构。</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">负载均衡器：它将传入的流量分配到多个后端服务之间。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">CDN（内容分发网络）：CDN 是一组地理分布的服务器，用于保存静态内容以便更快地进行分发。客户端首先在 CDN 中查找内容，然后再转到后端服务。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">API 网关：处理传入请求并将其路由到相关服务。它与身份提供者和服务发现进行对话。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">身份提供者：负责处理用户的身份验证和授权。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">服务注册和发现：微服务注册和发现发生在此组件中，API 网关在此组件中寻找相关服务进行通信。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">管理：此组件负责监控服务。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">微服务：微服务在不同的域中设计和部署。每个域都有自己的数据库。API 网关通过 REST API 或其他协议与微服务通信，同一域内的微服务使用 RPC（远程过程调用）相互通信。</font></font></li>
</ul>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">微服务的好处：</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">它们可以快速设计、部署和水平扩展。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">每个域可以由专门的团队独立维护。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">因此，可以在每个领域定制业务需求并获得更好的支持。</font></font></li>
</ul>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">微服务最佳实践</font></font></h3><a id="user-content-microservice-best-practices" class="anchor" aria-label="永久链接：微服务最佳实践" href="#microservice-best-practices"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">一图胜千言：开发微服务的 9 个最佳实践。</font></font></p>
<p dir="auto">
  <a target="_blank" rel="noopener noreferrer" href="/ByteByteGoHq/system-design-101/blob/main/images/microservice-best-practices.jpeg"><img src="/ByteByteGoHq/system-design-101/raw/main/images/microservice-best-practices.jpeg" style="max-width: 100%;"></a>
</p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在开发微服务时，我们需要遵循以下最佳实践：</font></font></p>
<ol dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">为每个微服务使用单独的数据存储</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">保持代码处于相似的成熟度级别</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">为每个微服务单独构建</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">为每个微服务分配单一职责</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">部署到容器中</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">设计无状态服务</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">采用领域驱动设计</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">设计微前端</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">编排微服务</font></font></li>
</ol>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">微服务通常使用什么技术栈？</font></font></h3><a id="user-content-what-tech-stack-is-commonly-used-for-microservices" class="anchor" aria-label="永久链接：微服务通常使用什么技术栈？" href="#what-tech-stack-is-commonly-used-for-microservices"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">下面您将看到一个显示微服务技术堆栈的图表，包括开发阶段和生产阶段。</font></font></p>
<p dir="auto">
  <a target="_blank" rel="noopener noreferrer" href="/ByteByteGoHq/system-design-101/blob/main/images/microservice-tech.jpeg"><img src="/ByteByteGoHq/system-design-101/raw/main/images/microservice-tech.jpeg" style="max-width: 100%;"></a>
</p>
<p dir="auto"><g-emoji class="g-emoji" alias="arrow_forward"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">▶️</font></font></g-emoji><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">𝐏𝐫𝐞-𝐏𝐫𝐨𝐝𝐮𝐜𝐭𝐢𝐨𝐧</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">定义 API - 这会在前端和后端之间建立契约。我们可以为此使用 Postman 或 OpenAPI。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">开发 - Node.js 或 react 适用于前端开发，java/python/go 适用于后端开发。此外，我们需要根据 API 定义更改 API 网关中的配置。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">持续集成 - JUnit 和 Jenkins 用于自动化测试。代码打包成 Docker 镜像并部署为微服务。</font></font></li>
</ul>
<p dir="auto"><g-emoji class="g-emoji" alias="arrow_forward"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">▶️</font></font></g-emoji><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">藝術本身</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">NGinx 是负载均衡器的常见选择。Cloudflare 提供 CDN（内容分发网络）。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">API 网关 - 我们可以使用 spring boot 作为网关，并使用 Eureka/Zookeeper 进行服务发现。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">微服务部署在云上。我们有 AWS、Microsoft Azure 或 Google GCP 可供选择。缓存和全文搜索 - Redis 是缓存键值对的常见选择。Elasticsearch 用于全文搜索。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">通信 - 为了使服务相互通信，我们可以使用消息传递基础设施 Kafka 或 RPC。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">持久性 - 我们可以使用 MySQL 或 PostgreSQL 作为关系数据库，使用 Amazon S3 作为对象存储。如有必要，我们还可以使用 Cassandra 作为宽列存储。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">管理和监控——为了管理如此多的微服务，常见的 Ops 工具包括 Prometheus、Elastic Stack 和 Kubernetes。</font></font></li>
</ul>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">为什么Kafka速度很快</font></font></h3><a id="user-content-why-is-kafka-fast" class="anchor" aria-label="永久链接：Kafka 为何如此快速" href="#why-is-kafka-fast"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">很多设计决策都对 Kafka 的性能产生了影响。在这篇文章中，我们将重点介绍其中两个。我们认为这两个是最重要的。</font></font></p>
<p dir="auto">
  <a target="_blank" rel="noopener noreferrer" href="/ByteByteGoHq/system-design-101/blob/main/images/why_is_kafka_fast.jpeg"><img src="/ByteByteGoHq/system-design-101/raw/main/images/why_is_kafka_fast.jpeg" style="max-width: 100%;"></a>
</p>
<ol dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">第一个是 Kafka 对顺序 I/O 的依赖。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使Kafka具有性能优势的第二个设计选择是其对效率的关注：零拷贝原则。</font></font></li>
</ol>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">该图说明了数据在生产者和消费者之间是如何传输的，以及零拷贝的含义。</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">步骤1.1 - 1.3：生产者将数据写入磁盘</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">步骤2：消费者不使用零拷贝读取数据</font></font></li>
</ul>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">2.1 数据从磁盘加载到OS缓存</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">2.2 数据从OS缓存复制到Kafka应用程序</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">2.3 Kafka应用程序将数据复制到套接字缓冲区中</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">2.4 数据从socket缓冲区复制到网卡</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">2.5 网卡向消费者发送数据</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">步骤3：消费者使用零拷贝读取数据</font></font></li>
</ul>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">3.1：数据从磁盘加载到OS缓存中 3.2 OS缓存通过sendfile()命令直接将数据复制到网卡 3.3 网卡将数据发送给消费者</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">零拷贝是节省应用程序上下文和内核上下文之间多次数据拷贝的一条捷径。</font></font></p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">支付系统</font></font></h2><a id="user-content-payment-systems" class="anchor" aria-label="永久链接：支付系统" href="#payment-systems"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如何学习支付系统？</font></font></h3><a id="user-content-how-to-learn-payment-systems" class="anchor" aria-label="永久链接：如何学习支付系统？" href="#how-to-learn-payment-systems"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto">
  <a target="_blank" rel="noopener noreferrer" href="/ByteByteGoHq/system-design-101/blob/main/images/learn-payments.jpg"><img src="/ByteByteGoHq/system-design-101/raw/main/images/learn-payments.jpg" style="max-width: 100%;"></a>
</p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">信用卡为何被称为“银行最赚钱的产品”？VISA/Mastercard如何赚钱？</font></font></h3><a id="user-content-why-is-the-credit-card-called-the-most-profitable-product-in-banks-how-does-visamastercard-make-money" class="anchor" aria-label="Permalink: 信用卡为何被称为“银行最赚钱的产品”？VISA/Mastercard 是如何赚钱的？" href="#why-is-the-credit-card-called-the-most-profitable-product-in-banks-how-does-visamastercard-make-money"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">下图显示了信用卡支付流程的经济原理。</font></font></p>
<p dir="auto">
  <a target="_blank" rel="noopener noreferrer" href="/ByteByteGoHq/system-design-101/blob/main/images/how does visa makes money.jpg"><img src="/ByteByteGoHq/system-design-101/raw/main/images/how does visa makes money.jpg" style="width: 640px; max-width: 100%;"></a>
</p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">1. 持卡人向商家支付 100 美元购买产品。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">2. 商户因使用销售量较高的信用卡而获益，并需要向发卡机构和信用卡网络支付提供支付服务的报酬。收单银行向商户收取一笔费用，称为“商户折扣费”。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">3 - 4. 收单银行保留 0.25 美元作为收单加价，并向发卡银行支付 1.75 美元作为交换费。商户折扣费应涵盖交换费。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">交换费由卡网络设定，因为每家发卡银行与每个商家协商费用效率较低。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">5. 信用卡网络与各家银行设定网络评估和费用，银行每月向信用卡网络支付服务费。例如，VISA 每次刷卡收取 0.11% 的评估费，外加 0.0195 美元的使用费。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">6.持卡人向发卡银行支付服务费用。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">开证行为什么要得到赔偿？</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">即使持卡人未能向发卡机构付款，发卡机构也会向商家付款。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">发卡机构先向商家付款，然后持卡人再向发卡机构付款。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">发行人还有其他运营成本，包括管理客户账户、提供报表、欺诈检测、风险管理、清算和结算等。</font></font></li>
</ul>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">当我们在商家店铺刷信用卡时 VISA 如何运作？</font></font></h3><a id="user-content-how-does-visa-work-when-we-swipe-a-credit-card-at-a-merchants-shop" class="anchor" aria-label="永久链接：当我们在商家店铺刷信用卡时，VISA 如何运作？" href="#how-does-visa-work-when-we-swipe-a-credit-card-at-a-merchants-shop"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto">
  <a target="_blank" rel="noopener noreferrer" href="/ByteByteGoHq/system-design-101/blob/main/images/visa_payment.jpeg"><img src="/ByteByteGoHq/system-design-101/raw/main/images/visa_payment.jpeg" style="max-width: 100%;"></a>
</p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">VISA、万事达卡和美国运通卡充当资金清算和结算的卡网络。卡收单银行和发卡银行可能（而且通常是）不同。如果银行在没有中介的情况下逐一结算交易，则每家银行都必须与所有其他银行结算交易。这非常低效。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">下图显示了 VISA 在信用卡支付流程中的作用。涉及两个流程。授权流程发生在客户刷信用卡时。捕获和结算流程发生在商家希望在一天结束时获得款项时。</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">授权流程</font></font></li>
</ul>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">步骤0：发卡银行向客户发行信用卡。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">步骤1：持卡人想要购买产品，并在商家店铺的销售点（POS）终端上刷信用卡。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">第 2 步：POS 终端将交易发送给提供 POS 终端的收单银行。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">步骤 3 和 4：收单银行将交易发送至信用卡网络（也称为信用卡计划）。信用卡网络将交易发送至发卡银行进行审批。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">步骤 4.1、4.2 和 4.3：如果交易获得批准，发卡银行将冻结资金。批准或拒绝信息将发送回收单机构和 POS 终端。</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">捕获和结算流程</font></font></li>
</ul>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">步骤 1 和 2：商户希望在一天结束时收款，因此他们在 POS 终端上点击“收款”。交易将批量发送给收单机构。收单机构将包含交易的批处理文件发送到卡网络。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">步骤3：卡网络对从不同收单机构收集的交易进行清算，并将清算文件发送给不同的发卡银行。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">步骤4：发卡银行确认清算文件的正确性，并将款项转入相关收单银行。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">第 5 步：收单银行随后将资金转入商户银行。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">步骤 4：信用卡网络清算来自不同收单银行的交易。清算是将相互抵消的交易进行净额结算的过程，因此总交易数量会减少。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在此过程中，信用卡网络承担与各家银行沟通的负担，并收取服务费。</font></font></p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">全球支付系统系列（第 1 部分）：印度的统一支付接口 (UPI)</font></font></h3><a id="user-content-payment-systems-around-the-world-series-part-1-unified-payments-interface-upi-in-india" class="anchor" aria-label="永久链接：全球支付系统系列（第 1 部分）：印度的统一支付接口 (UPI)" href="#payment-systems-around-the-world-series-part-1-unified-payments-interface-upi-in-india"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">什么是 UPI？UPI 是由印度国家支付公司开发的即时实时支付系统。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">它占目前印度数字零售交易的60%。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">UPI = 支付标记语言 + 可互操作支付标准</font></font></p>
<p dir="auto">
  <a target="_blank" rel="noopener noreferrer" href="/ByteByteGoHq/system-design-101/blob/main/images/how-does-upi-work.png"><img src="/ByteByteGoHq/system-design-101/raw/main/images/how-does-upi-work.png" style="width: 600px; max-width: 100%;"></a>
</p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">DevOps</font></font></h2><a id="user-content-devops" class="anchor" aria-label="永久链接：DevOps" href="#devops"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">DevOps 与 SRE 与平台工程。有什么区别？</font></font></h3><a id="user-content-devops-vs-sre-vs-platform-engineering-what-is-the-difference" class="anchor" aria-label="永久链接：DevOps 与 SRE 与平台工程。有什么区别？" href="#devops-vs-sre-vs-platform-engineering-what-is-the-difference"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">DevOps、SRE 和平台工程的概念出现在不同的时期，并由不同的个人和组织开发。</font></font></p>
<p dir="auto">
  <a target="_blank" rel="noopener noreferrer" href="/ByteByteGoHq/system-design-101/blob/main/images/devops-sre-platform.jpg"><img src="/ByteByteGoHq/system-design-101/raw/main/images/devops-sre-platform.jpg" style="max-width: 100%;"></a>
</p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">DevOps 的概念由 Patrick Debois 和 Andrew Shafer 于 2009 年在 Agile 大会上提出。他们试图通过促进协作文化和整个软件开发生命周期的共担责任来弥合软件开发和运营之间的差距。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">SRE（即站点可靠性工程）是 Google 在 21 世纪初率先推出的，旨在解决管理大型复杂系统时遇到的运营挑战。Google 开发了 SRE 实践和工具，例如 Borg 集群管理系统和 Monarch 监控系统，以提高其服务的可靠性和效率。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">平台工程是一个较新的概念，建立在 SRE 工程的基础上。平台工程的确切起源尚不清楚，但一般认为它是 DevOps 和 SRE 实践的延伸，重点是为产品开发提供一个支持整个业务视角的综合平台。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">值得注意的是，虽然这些概念出现的时间不同，但它们都与提高软件开发和运营的协作、自动化和效率的更广泛趋势相关。</font></font></p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">什么是 k8s (Kubernetes)？</font></font></h3><a id="user-content-what-is-k8s-kubernetes" class="anchor" aria-label="永久链接：什么是 k8s (Kubernetes)？" href="#what-is-k8s-kubernetes"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">K8s是一个容器编排系统，用于容器的部署和管理，其设计受到Google内部系统Borg的很大影响。</font></font></p>
<p dir="auto">
  <a target="_blank" rel="noopener noreferrer" href="/ByteByteGoHq/system-design-101/blob/main/images/k8s.jpeg"><img src="/ByteByteGoHq/system-design-101/raw/main/images/k8s.jpeg" style="width: 680px; max-width: 100%;"></a>
</p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">K8s 集群由一组运行容器化应用程序的工作机器（称为节点）组成。每个集群至少有一个工作节点。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">工作节点托管作为应用程序工作负载组件的 Pod。控制平面管理集群中的工作节点和 Pod。在生产环境中，控制平面通常跨多台计算机运行，而集群通常运行多个节点，以提供容错能力和高可用性。</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">控制平面组件</font></font></li>
</ul>
<ol dir="auto">
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">API 服务器</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">API 服务器与 k8s 集群中的所有组件进行通信。Pod 上的所有操作都是通过与 API 服务器通信来执行的。</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">调度器</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">调度程序监视 pod 工作负载并将负载分配到新创建的 pod 上。</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">控制器管理器</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">控制器管理器运行控制器，包括Node Controller、Job Controller、EndpointSlice Controller、ServiceAccount Controller。</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Etcd</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">etcd 是一个键值存储，用作 Kubernetes 所有集群数据的后备存储。</font></font></p>
</li>
</ol>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">节点</font></font></li>
</ul>
<ol dir="auto">
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Pod</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Pod 是一组容器，是 K8S 管理的最小单位。Pod 内的每个容器都应用一个 IP 地址。</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">库贝莱特</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在集群中的每个节点上运行的代理。它确保容器在 Pod 中运行。</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Kube 代理</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Kube-proxy 是运行在集群中每个节点上的网络代理。它路由从服务进入节点的流量。它将工作请求转发到正确的容器。</font></font></p>
</li>
</ol>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Docker 与 Kubernetes。我们应该使用哪一个？</font></font></h3><a id="user-content-docker-vs-kubernetes-which-one-should-we-use" class="anchor" aria-label="永久链接：Docker 与 Kubernetes。我们应该使用哪一个？" href="#docker-vs-kubernetes-which-one-should-we-use"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto">
  <a target="_blank" rel="noopener noreferrer" href="/ByteByteGoHq/system-design-101/blob/main/images/docker-vs-k8s.jpg"><img src="/ByteByteGoHq/system-design-101/raw/main/images/docker-vs-k8s.jpg" style="width: 680px; max-width: 100%;"></a>
</p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">什么是 Docker？</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Docker 是一个开源平台，可让您在隔离的容器中打包、分发和运行应用程序。它专注于容器化，提供封装应用程序及其依赖项的轻量级环境。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">什么是 Kubernetes？</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Kubernetes，通常称为 K8s，是一个开源容器编排平台。它提供了一个框架，用于自动在节点集群中部署、扩展和管理容器化应用程序。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">两者有何不同？</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Docker：Docker 在单个操作系统主机上的单个容器级别运行。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">您必须手动管理每个主机，并且为多个相关容器设置网络、安全策略和存储可能会很复杂。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Kubernetes：Kubernetes 在集群级别运行。它管理跨多个主机的多个容器化应用程序，并为负载平衡、扩展和确保应用程序所需状态等任务提供自动化。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">简而言之，Docker 专注于容器化和在单个主机上运行容器，而 Kubernetes 则专注于在主机集群中大规模管理和编排容器。</font></font></p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Docker 如何工作？</font></font></h3><a id="user-content-how-does-docker-work" class="anchor" aria-label="永久链接：Docker 如何工作？" href="#how-does-docker-work"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">下图展示了 Docker 的架构以及我们运行“docker build”、“docker pull”和“docker run”时它的工作原理。</font></font></p>
<p dir="auto">
  <a target="_blank" rel="noopener noreferrer" href="/ByteByteGoHq/system-design-101/blob/main/images/docker.jpg"><img src="/ByteByteGoHq/system-design-101/raw/main/images/docker.jpg" style="width: 680px; max-width: 100%;"></a>
</p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Docker 架构中有 3 个组件：</font></font></p>
<ul dir="auto">
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Docker 客户端</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Docker 客户端与 Docker 守护进程对话。</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Docker 主机</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Docker 守护进程监听 Docker API 请求并管理 Docker 对象，例如镜像、容器、网络和卷。</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Docker 注册表</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Docker 注册表存储 Docker 镜像。Docker Hub 是一个任何人都可以使用的公共注册表。</font></font></p>
</li>
</ul>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">我们以“docker run”命令为例。</font></font></p>
<ol dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Docker 从注册表中提取映像。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Docker 创建一个新容器。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Docker 为容器分配一个读写文件系统。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Docker 创建一个网络接口以将容器连接到默认网络。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Docker 启动容器。</font></font></li>
</ol>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">胃肠道疾病</font></font></h2><a id="user-content-git" class="anchor" aria-label="永久链接：GIT" href="#git"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Git 命令如何工作</font></font></h3><a id="user-content-how-git-commands-work" class="anchor" aria-label="永久链接：Git 命令如何工作" href="#how-git-commands-work"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">首先，必须确定我们的代码存储在哪里。通常的假设是只有两个位置 - 一个在 Github 等远程服务器上，另一个在我们的本地机器上。然而，这并不完全准确。Git 在我们的机器上维护三个本地存储，这意味着我们的代码可以在四个地方找到：</font></font></p>
<p dir="auto">
  <a target="_blank" rel="noopener noreferrer" href="/ByteByteGoHq/system-design-101/blob/main/images/git-commands.png"><img src="/ByteByteGoHq/system-design-101/raw/main/images/git-commands.png" style="width: 600px; max-width: 100%;"></a>
</p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">工作目录：我们编辑文件的地方</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">暂存区：保存文件以供下次提交的临时位置</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">本地存储库：包含已提交的代码</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">远程存储库：存储代码的远程服务器</font></font></li>
</ul>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">大多数 Git 命令主要在这四个位置之间移动文件。</font></font></p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Git 如何工作？</font></font></h3><a id="user-content-how-does-git-work" class="anchor" aria-label="永久链接：Git 如何工作？" href="#how-does-git-work"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">下图展示了 Git 工作流程。</font></font></p>
<p dir="auto">
  <a target="_blank" rel="noopener noreferrer" href="/ByteByteGoHq/system-design-101/blob/main/images/git-workflow.jpeg"><img src="/ByteByteGoHq/system-design-101/raw/main/images/git-workflow.jpeg" style="width: 520px; max-width: 100%;"></a>
</p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Git 是一个分布式版本控制系统。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">每个开发人员都会维护主存储库的本地副本，并编辑和提交本地副本。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">提交非常快，因为操作不与远程存储库交互。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如果远程存储库崩溃，则可以从本地存储库恢复文件。</font></font></p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Git merge 与 Git rebase</font></font></h3><a id="user-content-git-merge-vs-git-rebase" class="anchor" aria-label="永久链接：Git merge 与 Git rebase 对比" href="#git-merge-vs-git-rebase"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">有什么区别？</font></font></p>
<p dir="auto">
  <a target="_blank" rel="noopener noreferrer" href="/ByteByteGoHq/system-design-101/blob/main/images/git-merge-git-rebase.jpeg"><img src="/ByteByteGoHq/system-design-101/raw/main/images/git-merge-git-rebase.jpeg" style="width: 680px; max-width: 100%;"></a>
</p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">当我们</font></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">将更改</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">从一个 Git 分支合并到另一个 Git 分支时，我们可以使用“git merge”或“git rebase”。下图显示了这两个命令的工作原理。</font></font></p>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Git 合并</font></font></strong></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">这会在主分支中创建一个新的提交 G'。G' 将主分支和功能分支的历史记录联系起来。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Git 合并是非</font></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">破坏性的</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。主分支和功能分支都不会改变。</font></font></p>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Git 变基</font></font></strong></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Git rebase 将功能分支历史记录移至主分支的头部。它为功能分支中的每个提交创建新的提交 E'、F' 和 G'。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">rebase 的好处是它具有线性的</font></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">提交历史</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如果不遵循“git rebase 的黄金法则”，Rebase 可能会很危险。</font></font></p>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Git Rebase 的黄金法则</font></font></strong></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">切勿在公共分支上使用它！</font></font></p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">云服务</font></font></h2><a id="user-content-cloud-services" class="anchor" aria-label="固定链接：云服务" href="#cloud-services"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">不同云服务的实用小抄（2023 年版）</font></font></h3><a id="user-content-a-nice-cheat-sheet-of-different-cloud-services-2023-edition" class="anchor" aria-label="永久链接：不同云服务的速查表（2023 年版）" href="#a-nice-cheat-sheet-of-different-cloud-services-2023-edition"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto">
  <a target="_blank" rel="noopener noreferrer" href="/ByteByteGoHq/system-design-101/blob/main/images/cloud-compare.jpg"><img src="/ByteByteGoHq/system-design-101/raw/main/images/cloud-compare.jpg" style="max-width: 100%;"></a>
</p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">什么是云原生？</font></font></h3><a id="user-content-what-is-cloud-native" class="anchor" aria-label="永久链接：什么是云原生？" href="#what-is-cloud-native"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">下面的图表展示了 20 世纪 80 年代以来架构和流程的演变。</font></font></p>
<p dir="auto">
  <a target="_blank" rel="noopener noreferrer" href="/ByteByteGoHq/system-design-101/blob/main/images/cloud-native.jpeg"><img src="/ByteByteGoHq/system-design-101/raw/main/images/cloud-native.jpeg" style="width: 640px; max-width: 100%;"></a>
</p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">组织可以使用云原生技术在公共云、私有云和混合云上构建和运行可扩展的应用程序。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">这意味着这些应用程序旨在利用云功能，因此它们具有负载弹性并且易于扩展。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">云原生包括4个方面：</font></font></p>
<ol dir="auto">
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">开发过程</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">这已经从瀑布式发展到敏捷式，再发展到 DevOps。</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">应用程序架构</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">架构从单体架构演进为微服务架构，每个服务都设计得比较小，以适应云容器中有限的资源。</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">部署和打包</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">以前应用都是部署在物理服务器上，2000 年左右的时候，对延迟不敏感的应用一般都是部署在虚拟服务器上，而云原生应用则是打包成 docker 镜像，部署在容器里。</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">应用程序基础设施</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">应用程序大量部署在云基础设施上，而不是自托管服务器上。</font></font></p>
</li>
</ol>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">开发人员生产力工具</font></font></h2><a id="user-content-developer-productivity-tools" class="anchor" aria-label="永久链接：开发人员生产力工具" href="#developer-productivity-tools"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">可视化 JSON 文件</font></font></h3><a id="user-content-visualize-json-files" class="anchor" aria-label="永久链接：可视化 JSON 文件" href="#visualize-json-files"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">嵌套的 JSON 文件难以阅读。</font></font></p>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">JsonCrack</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">从 JSON 文件生成图表并使其易于阅读。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">此外，生成的图表可以作为图像下载。</font></font></p>
<p dir="auto">
  <a target="_blank" rel="noopener noreferrer" href="/ByteByteGoHq/system-design-101/blob/main/images/json-cracker.jpeg"><img src="/ByteByteGoHq/system-design-101/raw/main/images/json-cracker.jpeg" style="max-width: 100%;"></a>
</p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">自动将代码转化为架构图</font></font></h3><a id="user-content-automatically-turn-code-into-architecture-diagrams" class="anchor" aria-label="永久链接：自动将代码转换为架构图" href="#automatically-turn-code-into-architecture-diagrams"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto">
  <a target="_blank" rel="noopener noreferrer" href="/ByteByteGoHq/system-design-101/blob/main/images/diagrams_as_code.jpeg"><img src="/ByteByteGoHq/system-design-101/raw/main/images/diagrams_as_code.jpeg" style="width: 640px; max-width: 100%;"></a>
</p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">它有什么作用？</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">用Python代码绘制云系统架构。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">图表也可以直接在 Jupyter Notebook 中呈现。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">不需要任何设计工具。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">支持以下提供商：AWS、Azure、GCP、Kubernetes、阿里云、Oracle Cloud 等。</font></font></li>
</ul>
<p dir="auto"><a href="https://github.com/mingrammer/diagrams"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Github 仓库</font></font></a></p>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Linux</font></font></h2><a id="user-content-linux" class="anchor" aria-label="永久链接：Linux" href="#linux"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Linux 文件系统详解</font></font></h3><a id="user-content-linux-file-system-explained" class="anchor" aria-label="永久链接：Linux 文件系统详解" href="#linux-file-system-explained"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto">
  <a target="_blank" rel="noopener noreferrer" href="/ByteByteGoHq/system-design-101/blob/main/images/linux-file-systems.jpg"><img src="/ByteByteGoHq/system-design-101/raw/main/images/linux-file-systems.jpg" style="width: 680px; max-width: 100%;"></a>
</p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Linux 文件系统曾经就像一个杂乱无章的城镇，人们随意建造房屋。然而，1994 年，文件系统层次结构标准 (FHS) 的推出让 Linux 文件系统变得井然有序。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">通过实施 FHS 之类的标准，软件可以确保各种 Linux 发行版的布局一致。不过，并非所有 Linux 发行版都严格遵守此标准。它们通常包含自己独特的元素或满足特定要求。要熟练掌握此标准，您可以从探索开始。使用“cd”等命令进行导航，使用“ls”列出目录内容。将文件系统想象成一棵树，从根 (/) 开始。随着时间的推移，它将成为您的第二天性，让您成为一名熟练的 Linux 管理员。</font></font></p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">你应该知道的 18 个最常用的 Linux 命令</font></font></h3><a id="user-content-18-most-used-linux-commands-you-should-know" class="anchor" aria-label="永久链接：你应该知道的 18 个最常用的 Linux 命令" href="#18-most-used-linux-commands-you-should-know"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Linux 命令是与操作系统交互的指令。它们有助于管理文件、目录、系统进程和系统的许多其他方面。您需要熟悉这些命令才能高效、有效地导航和维护基于 Linux 的系统。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">下图显示了流行的 Linux 命令：</font></font></p>
<p dir="auto">
  <a target="_blank" rel="noopener noreferrer" href="/ByteByteGoHq/system-design-101/blob/main/images/18 Most-Used Linux Commands You Should Know-01.jpeg"><img src="/ByteByteGoHq/system-design-101/raw/main/images/18 Most-Used Linux Commands You Should Know-01.jpeg" style="width: 680px; max-width: 100%;"></a>
</p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ls——列出文件和目录</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">cd——更改当前目录</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">mkdir——创建新目录</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">rm——删除文件或目录</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">cp——复制文件或目录</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">mv——移动或重命名文件或目录</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">chmod——更改文件或目录的权限</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">grep——在文件中搜索模式</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">find——搜索文件和目录</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">tar——操作 tarball 存档文件</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">vi——使用文本编辑器编辑文件</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">cat—显示文件内容</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">top - 显示进程和资源使用情况</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ps—显示进程信息</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">kill——通过发送信号终止进程</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">du - 估计文件空间使用情况</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ifconfig——配置网络接口</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ping - 测试主机之间的网络连接</font></font></li>
</ul>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">安全</font></font></h2><a id="user-content-security" class="anchor" aria-label="固定链接：安全" href="#security"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">HTTPS 如何工作？</font></font></h3><a id="user-content-how-does-https-work" class="anchor" aria-label="永久链接：HTTPS 如何工作？" href="#how-does-https-work"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">超文本传输&ZeroWidthSpace;&ZeroWidthSpace;协议安全 (HTTPS) 是超文本传输&ZeroWidthSpace;&ZeroWidthSpace;协议 (HTTP) 的扩展。HTTPS 使用传输层安全性 (TLS) 传输加密数据。如果数据在线被劫持，劫持者得到的只是二进制代码。</font></font></p>
<p dir="auto">
  <a target="_blank" rel="noopener noreferrer" href="/ByteByteGoHq/system-design-101/blob/main/images/https.jpg"><img src="/ByteByteGoHq/system-design-101/raw/main/images/https.jpg" style="max-width: 100%;"></a>
</p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">数据如何加密和解密？</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">步骤 1-客户端（浏览器）和服务器建立 TCP 连接。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">第 2 步 - 客户端向服务器发送“客户端问候”。该消息包含一组必要的加密算法（密码套件）及其可以支持的最新 TLS 版本。服务器以“服务器问候”进行响应，以便浏览器知道它是否可以支持算法和 TLS 版本。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">然后，服务器将 SSL 证书发送给客户端。证书包含公钥、主机名、有效期等。客户端验证证书。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">步骤 3 - 验证 SSL 证书后，客户端生成会话密钥并使用公钥对其进行加密。服务器接收加密的会话密钥并使用私钥对其进行解密。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">步骤 4 - 现在客户端和服务器都持有相同的会话密钥（对称加密），加密数据在安全的双向通道中传输。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">为什么HTTPS在传输数据时要转为对称加密呢？主要有两个原因：</font></font></p>
<ol dir="auto">
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">安全性：非对称加密是单向的。这意味着，如果服务器尝试将加密数据发送回客户端，任何人都可以使用公钥解密数据。</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">服务器资源：非对称加密增加了相当多的数学开销。它不适合长时间会话的数据传输。</font></font></p>
</li>
</ol>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">用简单的术语解释 Oauth 2.0。</font></font></h3><a id="user-content-oauth-20-explained-with-simple-terms" class="anchor" aria-label="永久链接：用简单的术语解释 Oauth 2.0。" href="#oauth-20-explained-with-simple-terms"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">OAuth 2.0 是一个强大且安全的框架，允许不同的应用程序代表用户安全地相互交互，而无需共享敏感凭据。</font></font></p>
<p dir="auto">
  <a target="_blank" rel="noopener noreferrer" href="/ByteByteGoHq/system-design-101/blob/main/images/oAuth2.jpg"><img src="/ByteByteGoHq/system-design-101/raw/main/images/oAuth2.jpg" style="max-width: 100%;"></a>
</p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">OAuth 涉及的实体包括用户、服务器和身份提供者 (IDP)。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">OAuth 令牌能做什么？</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">使用 OAuth 时，您会获得一个代表您的身份和权限的 OAuth 令牌。此令牌可以执行一些重要操作：</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">单点登录 (SSO)：使用 OAuth 令牌，您只需一次登录即可登录多个服务或应用程序，让生活更轻松、更安全。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">跨系统授权：OAuth 令牌允许您在各个系统之间共享您的授权或访问权限，这样您不必在各个地方单独登录。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">访问用户配置文件：具有 OAuth 令牌的应用程序可以访问您允许的用户配置文件的某些部分，但无法看到所有内容。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">请记住，OAuth 2.0 的全部目的是确保您和您的数据安全，同时让您在不同的应用程序和服务之间获得无缝、无忧的在线体验。</font></font></p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">四种主要的身份验证机制</font></font></h3><a id="user-content-top-4-forms-of-authentication-mechanisms" class="anchor" aria-label="永久链接：四种最常用的身份验证机制" href="#top-4-forms-of-authentication-mechanisms"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto">
  <a target="_blank" rel="noopener noreferrer" href="/ByteByteGoHq/system-design-101/blob/main/images/top4-most-used-auth.jpg"><img src="/ByteByteGoHq/system-design-101/raw/main/images/top4-most-used-auth.jpg" style="max-width: 100%;"></a>
</p>
<ol dir="auto">
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">SSH 密钥：</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">加密密钥用于安全地访问远程系统和服务器</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">OAuth 令牌：</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">提供对第三方应用程序上的用户数据的有限访问权限的令牌</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">SSL 证书：</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">数字证书确保服务器和客户端之间的安全加密通信</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">证书：</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">用户身份验证信息用于验证和授予对各种系统和服务的访问权限</font></font></p>
</li>
</ol>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">会话、cookie、JWT、令牌、SSO 和 OAuth 2.0 - 它们是什么？</font></font></h3><a id="user-content-session-cookie-jwt-token-sso-and-oauth-20---what-are-they" class="anchor" aria-label="永久链接：会话、cookie、JWT、令牌、SSO 和 OAuth 2.0 - 它们是什么？" href="#session-cookie-jwt-token-sso-and-oauth-20---what-are-they"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">这些术语都与用户身份管理有关。当您登录网站时，您会声明自己的身份（身份证明）。您的身份会得到验证（身份验证），并且您会获得必要的权限（授权）。过去已经提出了许多解决方案，而且这个列表还在不断增加。</font></font></p>
<p dir="auto">
  <a target="_blank" rel="noopener noreferrer" href="/ByteByteGoHq/system-design-101/blob/main/images/session.jpeg"><img src="/ByteByteGoHq/system-design-101/raw/main/images/session.jpeg" style="max-width: 100%;"></a>
</p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">从简单到复杂，以下是我对用户身份管理的理解：</font></font></p>
<ul dir="auto">
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">WWW-Authenticate 是最基本的方法。浏览器会要求您输入用户名和密码。由于无法控制登录生命周期，因此现在很少使用这种方法。</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">对登录生命周期进行更精细的控制是 session-cookie。服务器维护会话存储，浏览器保留会话的 ID。cookie 通常仅适用于浏览器，不适用于移动应用。</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">为了解决兼容性问题，可以使用token，客户端将token发送给服务端，服务端验证token，缺点是token需要加密解密，耗时较长。</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">JWT 是代表 token 的标准方式。由于这些信息是经过数字签名的，因此可以验证和信任。由于 JWT 包含签名，因此无需在服务器端保存会话信息。</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">通过使用 SSO（单点登录），您只需登录一次即可登录多个网站。它使用 CAS（中央身份验证服务）来维护跨站点信息。</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">通过使用 OAuth 2.0，您可以授权一个网站访问您在另一个网站上的信息。</font></font></p>
</li>
</ul>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如何在数据库中安全地存储密码以及如何验证密码？</font></font></h3><a id="user-content-how-to-store-passwords-safely-in-the-database-and-how-to-validate-a-password" class="anchor" aria-label="永久链接：如何在数据库中安全地存储密码以及如何验证密码？" href="#how-to-store-passwords-safely-in-the-database-and-how-to-validate-a-password"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto">
  <a target="_blank" rel="noopener noreferrer" href="/ByteByteGoHq/system-design-101/blob/main/images/salt.jpg"><img src="/ByteByteGoHq/system-design-101/raw/main/images/salt.jpg" style="width: 720px; max-width: 100%;"></a>
</p>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">不该做的事情</font></font></strong></p>
<ul dir="auto">
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">以纯文本形式存储密码不是一个好主意，因为任何有内部访问权限的人都可以看到它们。</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">直接存储密码哈希值是不够的，因为它会被修剪以抵御预计算攻击，例如彩虹表。</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">为了减轻预计算攻击，我们对密码加盐。</font></font></p>
</li>
</ul>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">什么是盐？</font></font></strong></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">根据 OWASP 指南，“盐是一个唯一的、随机生成的字符串，作为散列过程的一部分添加到每个密码中”。</font></font></p>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如何存储密码和盐？</font></font></strong></p>
<ol dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">每个密码的哈希结果都是唯一的。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">密码可以使用以下格式存储在数据库中：hash（密码+盐）。</font></font></li>
</ol>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如何验证密码？</font></font></strong></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">为了验证密码，它可以经过以下过程：</font></font></p>
<ol dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">客户端输入密码。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">系统从数据库中获取相应的盐。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">系统将盐附加到密码中并进行哈希处理。我们将哈希值称为 H1。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">系统会比较H1和H2，其中H2是数据库中存储的哈希值。如果它们相同，则密码有效。</font></font></li>
</ol>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">向 10 岁的孩子解释 JSON Web Token (JWT)</font></font></h3><a id="user-content-explaining-json-web-token-jwt-to-a-10-year-old-kid" class="anchor" aria-label="永久链接：向 10 岁的孩子解释 JSON Web Token (JWT)" href="#explaining-json-web-token-jwt-to-a-10-year-old-kid"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto">
  <a target="_blank" rel="noopener noreferrer" href="/ByteByteGoHq/system-design-101/blob/main/images/jwt.jpg"><img src="/ByteByteGoHq/system-design-101/raw/main/images/jwt.jpg" style="max-width: 100%;"></a>
</p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">假设你有一个称为 JWT 的特殊盒子。这个盒子里面有三个部分：标头、有效负载和签名。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">标头就像是箱子外面的标签。它告诉我们箱子的类型以及它是如何固定的。它通常以一种称为 JSON 的格式编写，这只是一种使用花括号 { } 和冒号 : 来组织信息的方式。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">有效载荷就像您想要发送的实际消息或信息。它可以是您的姓名、年龄或您想要分享的任何其他数据。它也以 JSON 格式编写，因此易于理解和使用。现在，签名是使 JWT 安全的关键。它就像一个只有发送者知道如何创建的特殊印章。签名是使用秘密代码创建的，有点像密码。此签名可确保没有人可以在发送者不知情的情况下篡改 JWT 的内容。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">当您想要将 JWT 发送到服务器时，请将标头、有效负载和签名放入框中。然后将其发送到服务器。服务器可以轻松读取标头和有效负载，以了解您是谁以及您想要做什么。</font></font></p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Google Authenticator（或其他类型的双因素身份验证器）如何工作？</font></font></h3><a id="user-content-how-does-google-authenticator-or-other-types-of-2-factor-authenticators-work" class="anchor" aria-label="永久链接：Google Authenticator（或其他类型的双因素身份验证器）如何工作？" href="#how-does-google-authenticator-or-other-types-of-2-factor-authenticators-work"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Google Authenticator 是我们在启用双重身份验证时常用来登录账户的。它如何保证安全性？</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Google Authenticator 是一款基于软件的身份验证器，可实现两步验证服务。下图提供了详细信息。</font></font></p>
<p dir="auto">
  <a target="_blank" rel="noopener noreferrer" href="/ByteByteGoHq/system-design-101/blob/main/images/google_authenticate.jpeg"><img src="/ByteByteGoHq/system-design-101/raw/main/images/google_authenticate.jpeg" style="max-width: 100%;"></a>
</p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">涉及两个阶段：</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">第 1 阶段-用户启用 Google 两步验证。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">第 2 阶段-用户使用身份验证器进行登录等。</font></font></li>
</ul>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">让我们看看这些阶段。</font></font></p>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">阶段1</font></font></strong></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">步骤 1 和 2：Bob 打开网页启用两步验证。前端请求密钥。身份验证服务为 Bob 生成密钥并将其存储在数据库中。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">步骤3：认证服务返回一个URI给前端，该URI由密钥颁发者、用户名、密钥组成，以二维码的形式展示在网页上。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">步骤 4：Bob 然后使用 Google Authenticator 扫描生成的二维码。密钥存储在身份验证器中。</font></font></p>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">第 2 阶段</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
第 1 步和第 2 步：Bob 想要使用 Google 两步验证登录网站。为此，他需要密码。每 30 秒，Google Authenticator 会使用 TOTP（基于时间的一次性密码）算法生成一个 6 位密码。Bob 使用密码进入网站。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">步骤3和4：前端将Bob输入的密码发送到后端进行身份验证。身份验证服务从数据库读取密钥，并使用与客户端相同的TOTP算法生成6位密码。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">步骤5：认证服务比较客户端和服务器生成的两个密码，并将比较结果返回给前端。只有两个密码匹配，Bob才能继续登录过程。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">这种认证机制安全吗？</font></font></p>
<ul dir="auto">
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">密钥能否被别人获取？</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">我们需要确保密钥使用 HTTPS 传输。认证器客户端和数据库存储密钥，我们需要确保密钥是加密的。</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">6位数字的密码会被黑客猜到吗？</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">不是。密码有 6 位数字，所以生成的密码有 100 万种可能的组合。而且密码每 30 秒变化一次。如果黑客想在 30 秒内猜出密码，他们需要每秒输入 30,000 个组合。</font></font></p>
</li>
</ul>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">真实案例研究</font></font></h2><a id="user-content-real-world-case-studies" class="anchor" aria-label="永久链接：真实案例研究" href="#real-world-case-studies"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Netflix 的技术栈</font></font></h3><a id="user-content-netflixs-tech-stack" class="anchor" aria-label="永久链接：Netflix 的技术栈" href="#netflixs-tech-stack"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">本文基于众多 Netflix 工程博客和开源项目的研究。如果您发现任何不准确之处，请随时告知我们。</font></font></p>
<p dir="auto">
  <a target="_blank" rel="noopener noreferrer" href="/ByteByteGoHq/system-design-101/blob/main/images/netflix tech stack.png"><img src="/ByteByteGoHq/system-design-101/raw/main/images/netflix tech stack.png" style="width: 680px; max-width: 100%;"></a>
</p>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">移动和 Web</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：Netflix 采用 Swift 和 Kotlin 来构建原生移动应用。对于其 Web 应用，它使用 React。</font></font></p>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">前端/服务器通信</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：Netflix 使用 GraphQL。</font></font></p>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">后端服务</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：Netflix 依赖于 ZUUL、Eureka、Spring Boot 框架和其他技术。</font></font></p>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">数据库</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：Netflix 使用 EV 缓存、Cassandra、CockroachDB 和其他数据库。</font></font></p>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">消息传递/流媒体</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：Netflix 采用 Apache Kafka 和 Fink 来实现消息传递和流媒体传输。</font></font></p>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">视频存储</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：Netflix 使用 S3 和 Open Connect 进行视频存储。</font></font></p>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">数据处理</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：Netflix 使用 Flink 和 Spark 进行数据处理，然后使用 Tableau 进行可视化。使用 Redshift 处理结构化数据仓库信息。</font></font></p>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">CI/CD</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">：Netflix 采用各种工具（例如 JIRA、Confluence、PagerDuty、Jenkins、Gradle、Chaos Monkey、Spinnaker、Atlas 等）进行 CI/CD 流程。</font></font></p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Twitter 架构 2022</font></font></h3><a id="user-content-twitter-architecture-2022" class="anchor" aria-label="永久链接：Twitter 架构 2022" href="#twitter-architecture-2022"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">是的，这是真正的 Twitter 架构。它由 Elon Musk 发布，我们重新绘制以提高可读性。</font></font></p>
<p dir="auto">
  <a target="_blank" rel="noopener noreferrer" href="/ByteByteGoHq/system-design-101/blob/main/images/twitter-arch.jpeg"><img src="/ByteByteGoHq/system-design-101/raw/main/images/twitter-arch.jpeg" style="max-width: 100%;"></a>
</p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Airbnb 微服务架构 15 年的演进</font></font></h3><a id="user-content-evolution-of-airbnbs-microservice-architecture-over-the-past-15-years" class="anchor" aria-label="永久链接：Airbnb 微服务架构 15 年来的演变" href="#evolution-of-airbnbs-microservice-architecture-over-the-past-15-years"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Airbnb 的微服务架构经历了 3 个主要阶段。</font></font></p>
<p dir="auto">
  <a target="_blank" rel="noopener noreferrer" href="/ByteByteGoHq/system-design-101/blob/main/images/airbnb_arch.jpeg"><img src="/ByteByteGoHq/system-design-101/raw/main/images/airbnb_arch.jpeg" style="max-width: 100%;"></a>
</p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">巨石（2008 - 2017）</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Airbnb 最初是一个简单的房东和房客市场。它建立在 Ruby on Rails 应用程序（整体式）中。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">有什么挑战？</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">令人困惑的团队所有权 + 无人拥有的代码</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">部署缓慢</font></font></li>
</ul>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">微服务（2017 - 2020）</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">微服务旨在解决这些挑战。在微服务架构中，关键服务包括：</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">数据获取服务</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">业务逻辑数据服务</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">编写工作流服务</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">UI聚合服务</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">每个服务都有一个负责团队</font></font></li>
</ul>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">有什么挑战？</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">数以百计的服务和依赖关系对于人类来说很难管理。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">微服务 + 宏服务（2020 年至今）</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">这也是Airbnb现在在做的，微宏服务混合模式，重在API的统一。</font></font></p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Monorepo 与 Microrepo。</font></font></h3><a id="user-content-monorepo-vs-microrepo" class="anchor" aria-label="永久链接：Monorepo 与 Microrepo。" href="#monorepo-vs-microrepo"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">哪一个最好？为什么不同的公司会做出不同的选择？</font></font></p>
<p dir="auto">
  <a target="_blank" rel="noopener noreferrer" href="/ByteByteGoHq/system-design-101/blob/main/images/monorepo-microrepo.jpg"><img src="/ByteByteGoHq/system-design-101/raw/main/images/monorepo-microrepo.jpg" style="max-width: 100%;"></a>
</p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Monorepo 并非新鲜事物；Linux 和 Windows 都是使用 Monorepo 创建的。为了提高可扩展性和构建速度，Google 开发了其内部专用工具链以更快地扩展它，并制定了严格的编码质量标准以保持一致性。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">亚马逊和 Netflix 是微服务理念的主要倡导者。这种方法自然地将服务代码分离到单独的存储库中。它的扩展速度更快，但以后可能会导致治理难题。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在 Monorepo 中，每个服务都是一个文件夹，每个文件夹都有 BUILD 配置和 OWNERS 权限控制，每个服务成员负责自己的文件夹。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">另一方面，在 Microrepo 中，每个服务负责其存储库，并通常为整个存储库设置构建配置和权限。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在 Monorepo 中，无论您的业务如何，依赖项都会在整个代码库中共享，因此当版本升级时，每个代码库都会升级其版本。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">在 Microrepo 中，依赖关系在每个存储库内进行控制。企业可以根据自己的时间表选择何时升级版本。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Monorepo 有一个签入标准。Google 的代码审查流程以设定高标准而闻名，无论业务如何，它都能确保 Monorepo 具有一致的质量标准。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Microrepo 可以制定自己的标准，也可以通过整合最佳实践来采用共享标准。它可以更快地扩展业务，但代码质量可能会有所不同。Google 工程师构建了 Bazel，Meta 构建了 Buck。还有其他开源工具可用，包括 Nx、Lerna 等。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">多年来，Microrepo 已经支持更多的工具，包括 Java 的 Maven 和 Gradle、NodeJS 的 NPM 以及 C/C++ 的 CMake 等。</font></font></p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">您将如何设计 Stack Overflow 网站？</font></font></h3><a id="user-content-how-will-you-design-the-stack-overflow-website" class="anchor" aria-label="永久链接：您将如何设计 Stack Overflow 网站？" href="#how-will-you-design-the-stack-overflow-website"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">如果您的答案是内部部署服务器和整体式架构（在下图的底部），那么您很可能会在面试中失败，但这就是它在现实中的构建方式！</font></font></p>
<p dir="auto">
  <a target="_blank" rel="noopener noreferrer" href="/ByteByteGoHq/system-design-101/blob/main/images/stackoverflow.jpg"><img src="/ByteByteGoHq/system-design-101/raw/main/images/stackoverflow.jpg" style="max-width: 100%;"></a>
</p>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">人们认为它应该是什么样的</font></font></strong></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">面试官可能期望看到类似图片的上半部分。</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">微服务用于将系统分解为小的组件。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">每个服务都有自己的数据库。大量使用缓存。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">该服务是分片的。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">服务之间通过消息队列异步地互相通信。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">该服务是使用 CQRS 事件源实现的。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">展示分布式系统知识，例如最终一致性、CAP 定理等。</font></font></li>
</ul>
<p dir="auto"><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">它到底是什么</font></font></strong></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Stack Overflow 仅使用 9 台内部 Web 服务器来处理所有流量，而且它是一个整体！它有自己的服务器，不在云端运行。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">这与我们现今的所有流行信念相悖。</font></font></p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">为什么 Amazon Prime Video 监控从无服务器转向单片机？它如何节省 90% 的成本？</font></font></h3><a id="user-content-why-did-amazon-prime-video-monitoring-move-from-serverless-to-monolithic-how-can-it-save-90-cost" class="anchor" aria-label="永久链接：为什么 Amazon Prime Video 监控从无服务器转向单片机？它如何节省 90% 的成本？" href="#why-did-amazon-prime-video-monitoring-move-from-serverless-to-monolithic-how-can-it-save-90-cost"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">下图是迁移前后的架构对比。</font></font></p>
<p dir="auto">
  <a target="_blank" rel="noopener noreferrer" href="/ByteByteGoHq/system-design-101/blob/main/images/serverless-to-monolithic.jpeg"><img src="/ByteByteGoHq/system-design-101/raw/main/images/serverless-to-monolithic.jpeg" style="max-width: 100%;"></a>
</p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">什么是 Amazon Prime 视频监控服务？</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Prime Video 服务需要监控数千个直播流的质量。监控工具会自动实时分析直播流并识别诸如块损坏、视频冻结和同步问题等质量问题。这是确保客户满意度的重要过程。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">共有 3 个步骤：媒体转换器、缺陷检测器和实时通知。</font></font></p>
<ul dir="auto">
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">旧架构存在什么问题？</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">旧架构基于 Amazon Lambda，这有利于快速构建服务。但是，在大规模运行架构时，它并不划算。最昂贵的两个操作是：</font></font></p>
</li>
</ul>
<ol dir="auto">
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">编排工作流 - AWS 步骤函数通过状态转换向用户收费，并且编排每秒执行多次状态转换。</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">分布式组件之间的数据传递 - 中间数据存储在 Amazon S3 中，以便下一阶段下载。当下载量很大时，下载成本可能很高。</font></font></p>
</li>
</ol>
<ul dir="auto">
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">整体架构节省 90% 成本</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">为解决成本问题，我们设计了单片架构。虽然仍然有 3 个组件，但媒体转换器和缺陷检测器部署在同一流程中，从而节省了通过网络传输数据的成本。令人惊讶的是，这种部署架构更改方法可节省 90% 的成本！</font></font></p>
</li>
</ul>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">这是一个有趣而独特的案例研究，因为微服务已成为科技行业的首选和时尚选择。很高兴看到我们正在就架构的发展进行更多讨论，并就其优缺点进行更坦诚的讨论。将组件分解为分布式微服务需要付出代价。</font></font></p>
<ul dir="auto">
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">亚马逊领导层对此有何看法？</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">亚马逊首席技术官 Werner Vogels：“构建</font></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">可进化的软件系统</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">是一种策略，而不是一种宗教。必须以开放的心态重新审视您的架构。”</font></font></p>
</li>
</ul>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">前亚马逊可持续发展副总裁 Adrian Cockcroft：“Prime Video 团队遵循了一条我称之为</font></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">‘无服务器优先’</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">的道路……我不主张</font></font><strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">‘仅无服务器</font></font></strong><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">’”。</font></font></p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Disney Hotstar 如何在比赛期间捕捉 50 亿个表情符号？</font></font></h3><a id="user-content-how-does-disney-hotstar-capture-5-billion-emojis-during-a-tournament" class="anchor" aria-label="永久链接：Disney Hotstar 如何在锦标赛期间捕捉 50 亿个表情符号？" href="#how-does-disney-hotstar-capture-5-billion-emojis-during-a-tournament"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto">
  <a target="_blank" rel="noopener noreferrer" href="/ByteByteGoHq/system-design-101/blob/main/images/hotstar_emojis.jpeg"><img src="/ByteByteGoHq/system-design-101/raw/main/images/hotstar_emojis.jpeg" style="width: 720px; max-width: 100%;"></a>
</p>
<ol dir="auto">
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">客户端通过标准 HTTP 请求发送表情符号。你可以将 Golang Service 视为典型的 Web 服务器。之所以选择 Golang，是因为它能够很好地支持并发。Golang 中的线程是轻量级的。</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">由于写入量很大，所以使用Kafka（消息队列）作为缓冲。</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">表情符号数据由名为 Spark 的流处理服务聚合。它每 2 秒聚合一次数据，该频率是可配置的。需要根据间隔做出权衡。较短的间隔意味着表情符号可以更快地传递给其他客户端，但这也意味着需要更多的计算资源。</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">聚合数据被写入另一个 Kafka。</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">PubSub 消费者从 Kafka 提取聚合的表情符号数据。</font></font></p>
</li>
<li>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">表情符号通过 PubSub 基础架构实时传递给其他客户端。PubSub 基础架构很有趣。Hotstar 考虑了以下协议：Socketio、NATS、MQTT 和 gRPC，最后选择了 MQTT。</font></font></p>
</li>
</ol>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">LinkedIn 采用了类似的设计，每秒可产生一百万个赞。</font></font></p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Discord 如何存储数万亿条消息</font></font></h3><a id="user-content-how-discord-stores-trillions-of-messages" class="anchor" aria-label="永久链接：Discord 如何存储数万亿条消息" href="#how-discord-stores-trillions-of-messages"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">下图展示了 Discord 消息存储的演变过程：</font></font></p>
<p dir="auto">
  <a target="_blank" rel="noopener noreferrer" href="/ByteByteGoHq/system-design-101/blob/main/images/discord-store-messages.jpg"><img src="/ByteByteGoHq/system-design-101/raw/main/images/discord-store-messages.jpg" style="max-width: 100%;"></a>
</p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">MongoDB➡️Cassandra➡️ScyllaDB</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">2015 年，Discord 的第一个版本基于单个 MongoDB 副本构建。2015 年 11 月左右，MongoDB 存储了 1 亿条消息，RAM 无法再容纳这些数据和索引。延迟变得不可预测。消息存储需要移至另一个数据库。最终选择了 Cassandra。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">2017 年，Discord 拥有 12 个 Cassandra 节点，存储了数十亿条消息。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">2022 年初，它拥有 177 个节点，包含数万亿条消息。此时，延迟不可预测，维护操作的成本太高。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">造成这一问题的原因有多种：</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Cassandra 使用 LSM 树作为内部数据结构。读取比写入更昂贵。在拥有数百名用户的服务器上可能会发生许多并发读取，从而导致热点。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">维护集群（例如压缩 SSTables）会影响性能。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">垃圾收集暂停会导致严重的延迟峰值</font></font></li>
</ul>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ScyllaDB 是用 C++ 编写的与 Cassandra 兼容的数据库。Discord 重新设计了其架构，使其拥有单一 API、用 Rust 编写的数据服务和基于 ScyllaDB 的存储。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">ScyllaDB 中的 p99 读取延迟为 15 毫秒，而 Cassandra 中的 p99 读取延迟为 40-125 毫秒。p99 写入延迟为 5 毫秒，而 Cassandra 中的 p99 写入延迟为 5 毫秒。</font></font></p>
<div class="markdown-heading" dir="auto"><h3 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">YouTube、TikTok Live 或 Twitch 上的视频直播如何进行？</font></font></h3><a id="user-content-how-do-video-live-streamings-work-on-youtube-tiktok-live-or-twitch" class="anchor" aria-label="永久链接：YouTube、TikTok Live 或 Twitch 上的视频直播如何进行？" href="#how-do-video-live-streamings-work-on-youtube-tiktok-live-or-twitch"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">直播不同于常规直播，因为视频内容是通过互联网实时发送的，通常延迟只有几秒钟。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">下图解释了实现这一目标的幕后过程。</font></font></p>
<p dir="auto">
  <a target="_blank" rel="noopener noreferrer" href="/ByteByteGoHq/system-design-101/blob/main/images/live_streaming_updated.jpg"><img src="/ByteByteGoHq/system-design-101/raw/main/images/live_streaming_updated.jpg" style="width: 640px; max-width: 100%;"></a>
</p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">步骤1：麦克风和摄像头捕获原始视频数据。数据被发送到服务器端。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">第二步：视频数据经过压缩编码。例如，压缩算法会将背景和其他视频元素分离。压缩后的视频会被编码为 H.264 等标准。经过这一步后，视频数据的大小会小很多。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">步骤 3：编码数据被分成更小的段，通常长度为几秒，因此下载或流式传输所需的时间要少得多。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">步骤 4：分段数据被发送到流媒体服务器。流媒体服务器需要支持不同的设备和网络条件。这称为“自适应比特率流媒体”。这意味着我们需要在步骤 2 和步骤 3 中以不同的比特率生成多个文件。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">第五步：直播数据被推送到CDN（内容分发网络）支持的边缘服务器。数百万观众可以从附近的边缘服务器观看视频。CDN 显著降低了数据传输延迟。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">第六步：观众的设备对视频数据进行解码、解压缩，并在视频播放器中播放视频。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">第 7 步和第 8 步：如果需要存储视频以供重播，则将编码数据发送到存储服务器，观众可以稍后向其请求重播。</font></font></p>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">直播的标准协议包括：</font></font></p>
<ul dir="auto">
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">RTMP（实时消息协议）：该协议最初由 Macromedia 开发，用于在 Flash 播放器和服务器之间传输数据。现在，它用于通过互联网传输视频数据。请注意，Skype 等视频会议应用程序使用 RTC（实时通信）协议来降低延迟。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">HLS（HTTP 实时流）：需要 H.264 或 H.265 编码。Apple 设备仅接受 HLS 格式。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">DASH（通过 HTTP 的动态自适应流）：DASH 不支持 Apple 设备。</font></font></li>
<li><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">HLS 和 DASH 都支持自适应比特率流。</font></font></li>
</ul>
<div class="markdown-heading" dir="auto"><h2 tabindex="-1" class="heading-element" dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">执照</font></font></h2><a id="user-content-license" class="anchor" aria-label="永久链接：许可证" href="#license"><svg class="octicon octicon-link" viewBox="0 0 16 16" version="1.1" width="16" height="16" aria-hidden="true"><path d="m7.775 3.275 1.25-1.25a3.5 3.5 0 1 1 4.95 4.95l-2.5 2.5a3.5 3.5 0 0 1-4.95 0 .751.751 0 0 1 .018-1.042.751.751 0 0 1 1.042-.018 1.998 1.998 0 0 0 2.83 0l2.5-2.5a2.002 2.002 0 0 0-2.83-2.83l-1.25 1.25a.751.751 0 0 1-1.042-.018.751.751 0 0 1-.018-1.042Zm-4.69 9.64a1.998 1.998 0 0 0 2.83 0l1.25-1.25a.751.751 0 0 1 1.042.018.751.751 0 0 1 .018 1.042l-1.25 1.25a3.5 3.5 0 1 1-4.95-4.95l2.5-2.5a3.5 3.5 0 0 1 4.95 0 .751.751 0 0 1-.018 1.042.751.751 0 0 1-1.042.018 1.998 1.998 0 0 0-2.83 0l-2.5 2.5a1.998 1.998 0 0 0 0 2.83Z"></path></svg></a></div>
<p dir="auto"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">本作品已获</font></font><a href="http://creativecommons.org/licenses/by-nc-nd/4.0/?ref=chooser-v1" rel="nofollow"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">CC BY-NC-ND 4.0许可</font></font><img style="height: 22px; max-width: 100%;" src="https://camo.githubusercontent.com/458ceef6cb2ac6b254b4b0dfd4e48377408aec5fac260cd05cd8b2d8ae7662ba/68747470733a2f2f6d6972726f72732e6372656174697665636f6d6d6f6e732e6f72672f70726573736b69742f69636f6e732f63632e7376673f7265663d63686f6f7365722d7631" data-canonical-src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1"><img style="height: 22px; max-width: 100%;" src="https://camo.githubusercontent.com/0facd73730f0913c3efc1c96c1b102dbca6ede745526e26314994d9811121f72/68747470733a2f2f6d6972726f72732e6372656174697665636f6d6d6f6e732e6f72672f70726573736b69742f69636f6e732f62792e7376673f7265663d63686f6f7365722d7631" data-canonical-src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1"><img style="height: 22px; max-width: 100%;" src="https://camo.githubusercontent.com/a68cb809bc229256e15bdfe2ec830decaca1ec797d908ebfb33940800b264d72/68747470733a2f2f6d6972726f72732e6372656174697665636f6d6d6f6e732e6f72672f70726573736b69742f69636f6e732f6e632e7376673f7265663d63686f6f7365722d7631" data-canonical-src="https://mirrors.creativecommons.org/presskit/icons/nc.svg?ref=chooser-v1"><img style="height: 22px; max-width: 100%;" src="https://camo.githubusercontent.com/9c2bab8f8fefdcc68b1b4127618b14739a7077c59eefa5c3855249c2e9bd8c14/68747470733a2f2f6d6972726f72732e6372656174697665636f6d6d6f6e732e6f72672f70726573736b69742f69636f6e732f6e642e7376673f7265663d63686f6f7365722d7631" data-canonical-src="https://mirrors.creativecommons.org/presskit/icons/nd.svg?ref=chooser-v1"></a></p>
</article></div>
